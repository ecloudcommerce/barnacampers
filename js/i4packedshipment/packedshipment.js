/**
* Funciones js para la funcionalidad de envio por bultos agrupado
*
**/


var ps_totalItemsToShip = 0;
var ps_qtyInputs;
var ps_bultosInput;
var ps_edit_form;
var ps_data;
var runAddressValidation = true;
var modalDialog1;

var i4PSVars = {
    modalDialog1Title: "Distribución de bultos",
    bultoColumnTitle : "bulto ",
    cancelLabel: "cancelar",
    continueLabel: "continuar",
    submitLabel: "enviar",
    useDescriptionsInsteadOfReferences: false,
    referenciaBultoLabel: "Referencia bulto ",
    bultoDescriptionLabel: "Descripción del bulto "
}



/**
 * Vamos a extender la clase varienps_edit_form para que el submit lance un evento que podamos capturar
 *
 **/
//when subclassing, specify the class you want to inherit from
var varienForm = Class.create(varienForm, {
  // redefine the submit method
  submit: function($super, url, doSubmit) {
	  Event.fire($(this.formId), 'packedshipment:submit');
	  if(!doSubmit)
	  {
		  return false;
	  } else {
		  return $super(url);  
	  }
  }
});



function initNumBultos()
{
	ps_qtyInputs = $$("#ship_items_container .grid input");
	ps_qtyInputs.each(function(input){
		Event.observe(input, 'change', updateMaxBultos);
	});
	ps_bultosInput = $("packedshipment_num_bultos");
	Event.observe(ps_bultosInput, 'change', validateps_bultosInput);
	
	ps_edit_form = $('edit_form');
	
	updateMaxBultos();
}

/**
 * Contabiliza los items totales que hay para enviar
 */
function updateMaxBultos()
{
    // En el caso de que las reglas del transportista nos obliguen a enviar
    // todo el envío en un solo bulto, dejamos desactivado el campo de seleccionar el 
    // número máximo de bultos y con el máximo por defecto que es 1.
    // En el caso contrario, inicializamos este campo con el número total de 
    // artículos que contiene el envío.
    
    if (!mustSendShipmentInOneBulto) { // mustSendShipmentInOneBulto definido en la plantilla items.phtml
	ps_totalItemsToShip = 0;
	ps_qtyInputs.each(function(input){
		ps_totalItemsToShip = parseInt(input.value) + ps_totalItemsToShip;
	});
	
	ps_bultosInput.value = ps_totalItemsToShip;
	ps_bultosInput.enable();
    }
    else {
        ps_bultosInput.value = 1;
        ps_bultosInput.disable();
    }
}

/**
 * 
 * validamos que el numero de bultos este entre cero y el numero de items total a enviar
 */
function validateps_bultosInput(event)
{
	//no puede ser mayor que el ps_totalItemsToShip
	//ni puede ser cero
	var valid = true;
	var input = event.target;
	var value = input.getValue();
	
	if(isNaN(value) || value < 1 || value > ps_totalItemsToShip)
	{	
		Dialog.alert("El número total de bultos no puede ser 0 ni mayor que el número de items a enviar.", 
			{
				width:300, 
				okLabel: "aceptar",
				className: "magento",
				ok:function(win) {
					Form.Element.activate(input);
					input.value = ps_totalItemsToShip;
					return true;
				}
			}
		);
	}
	
}




function _cancelModales()
{
	Event.fire(ps_edit_form, 'packedshipment:cancel');
	$$(".packedshipment_added").each(function(el){
		el.remove();
	});
}


function _doEditFormSubmit()
{
	//inyectamos como hiddens los campos para enviar la info de ps_data
	ps_data.each(function(el, index){
		//creamos un hidden por cada id de este bulto
		el.ids.each(function(id){
			var input = new Element('input', { 
				'type': 'hidden', 
				'name': 'packages['+ el.package+'][ids][]', 
				'value':id, 
				'class':'packedshipment_hidden' 
			});
			
			ps_edit_form.insert(input);
		});
	});
	
	
	//creamos el ref por cada bulto
	//solo si tienen items que enviar (ids)
	for(var i=0; i< ps_bultosInput.value ; i++)
	{
		var packageIndex = i + 1;
		var data = ps_data[i];
		if(data.ids.length > 0)
		{
			var input = new Element('input', { 
				'type': 'hidden', 
				'name': 'packages['+ packageIndex +'][ref]', 
				'value':ps_data[i].ref, 
				'class':'packedshipment_hidden' 
			});
		}	
		
		ps_edit_form.insert(input);
	}
	
	ps_edit_form.submit();
}

function _getTotalWeightAllBultos()
{
	// Calculamos el peso total de todos los bultos, que es siempre igual.
	var bultosWeightsArr = document.getElementsByName('lineWeight');
	var sumWeights = 0;
	for (var arrIdx = 0; arrIdx < bultosWeightsArr.length; arrIdx ++)
	{
	    sumWeights += parseFloat($(bultosWeightsArr[arrIdx]).innerHTML);
	}
	
	return sumWeights;
}

function _recalculateTotalBultoWeights()
{
    // Iniciamos los totales de peso para cada bulto como 0.
    var numBultos = ps_bultosInput.value;
    for (var bultoIdx=1; bultoIdx <= numBultos; bultoIdx++)
    {
	$('bulto_total_weight_' + bultoIdx).innerHTML = '0.0000';
    }	   
    
 
    
    // Calculamos los pesos totales de cada bulto.
    $$(modalDialog1 ? "#" + modalDialog1.getId() + " .packedshipment_input" : "input.packedshipment_input" ).each(function(el){
	if((el.type == 'radio') && (el.checked))
	{
	    var bultoIndex = el.getValue();
	    var eltName = el.readAttribute('name');
	    var eltNameParts =  eltName.split("_");
	    var productId = eltNameParts[0];
	    
	    // El peso de cada producto se encuentra al principio de cada fila en un elemento con id 
	    // lineWeight_<product_id>_<row_num>.
	    // Pueden existir varios elementos para productos con el mismo ID (que logicamente tendrán el mismo 
	    // peso) así que ubicamos el primero.
	    
	    var productWeigthEls = $$('[id^="lineWeight_'+ productId +'"]');
	    var productWeightEl = productWeigthEls[0];
	    var productWeight = parseFloat(productWeightEl.innerHTML);
	    var bultoTotalWeight = $('bulto_total_weight_' + bultoIndex);
	    bultoTotalWeight.innerHTML = (parseFloat(bultoTotalWeight.innerHTML) + productWeight).toFixed(4);
	}
    })    
}

// Si la interaz del distribuidor dispone de un servicio de calcular costes de 
// envío, lo calculamos basado en la configuración de bultos y código postal/población 
// actual.
// Importante: esta función requiere que ya hayamos calculado los pesos de los 
// bultos, realizando una llamada a la función _recalculateTotalBultoWeights.
function _recalculateShippingCost()
{
    if (!$('i4ShippingCostTable'))
    {
        return;
    }
 
    
    var ajaxParameters = [];
    ajaxParameters['order'] = i4ShipmentOrderId;
    
    // Si tenemos validación de direcciones, tendremos la población y la ciudad
    ajaxParameters['city'] = i4ShippingAddressCity ? i4ShippingAddressCity : '';
    ajaxParameters['postcode'] = i4ShippingAddressPostcode ? i4ShippingAddressPostcode : '';
    
    // Sacamos un array de los pesos de los bultos que actualmente contienen algo.
    $$(modalDialog1 ? "#" + modalDialog1.getId() + " .packedshipment_input" : "input.packedshipment_input" ).each(function(el){
	if((el.type == 'radio') && (el.checked))
	{
	    var bultoIndex = el.getValue();
            ajaxParameters['weightsBultos[' + bultoIndex + ']'] = $('bulto_total_weight_' + bultoIndex).innerHTML;
	}
    });
    
    
    
    new Ajax.Request(i4ShippingCostUrl, {
	method: 'post',
	parameters: ajaxParameters,
	onSuccess: function(transport){
	    var jsonResponse = transport.responseJSON;
            if (jsonResponse.shippingcost)
            {
                $('i4ShippingCost').innerHTML = jsonResponse.shippingcost;
                $('i4ShippingProfit').innerHTML = jsonResponse.profit;
                $('i4ShippingProfit').style.color = jsonResponse.profitcolor;
                $('i4ShippingResportsShippingCost').setValue(jsonResponse.shippingreportsshippingcost);
            }
        }
    });  
    
    
    
 
}

function _recalculateTotals()
{
    // Calculamos los totales actuales de peso y coste para cada bulto.
    _recalculateTotalBultoWeights();
    
    // Calculamos los costes de envío si la interfaz proporciona este servicio.
    _recalculateShippingCost();
}

function _prepareModal1()
{
	var numBultos = ps_bultosInput.value;
	
	for(var i=1; i <= numBultos; i++)
	{	
		//por cada bulto seleccionado tenemos que crear un th
		var th = new Element('th', {'align':'center', 'valign':'middle', 'class':'packedshipment_added', 'style':'padding:5px;'}).update(i4PSVars.bultoColumnTitle + i);
		$("packages_modal_1_headings_tr").insert(th);
	}
	
	//tengo que crear una línea de producto por cada producto a enviar, con la siguiente estructura html
//	<tbody class="even">
//	    <tr packages:item_id="123_1" class="border packages_modal_1_product_tr">
//	        <td>oc_sm</td>
//	        <td>The Only Children: Paisley T-Shirt</td>
//	        <td>0.4400</td>
//	    <td class="packedshipment_added"><input type="radio" name="123_1" value="1" class="packedshipment_input" checked="checked"></td><td class="packedshipment_added"><input type="radio" name="123_1" value="2" class="packedshipment_input"></td><td class="packedshipment_added"><input type="radio" name="123_1" value="3" class="packedshipment_input"></td></tr>
//    </tbody>	
	//para que el usuario elija en cual bulto va a incluir este producto
	var checkedIndex = 1;
	var row = 0;
	$$("#ship_items_container .grid input").each(function(el){
		var qty = el.getValue();
		var id = el.readAttribute('name').replace("shipment[items][", "");
		id = id.replace("]", "");
		ps_itemData = ps_items[id];
		
		
		for(var i = 0 ; i < qty; i++)
		{
			var sku = new Element('td', {'align':'center', 'valign':'middle', 'class':'packedshipment_added', 'style':'padding:5px;'});
			sku.update(ps_itemData.sku);
			
			var name = new Element('td', {'align':'center', 'valign':'middle', 'class':'packedshipment_added', 'style':'padding:5px;'});
			name.update(ps_itemData.name);
			
			var lineWeightId = 'lineWeight_' + ps_itemData.productId + '_' + row;
			var weight = new Element('td', {'id' : lineWeightId, name : 'lineWeight', 'align':'center', 'valign':'middle', 'class':'packedshipment_added', 'style':'padding:5px;'});
			weight.update(ps_itemData.weight);
			
			var tr = new Element('tr', {'class':'border packages_modal_1_product_tr', 'packages:item_id':(ps_itemData.productId + '_' + (row+1))});
			tr.insert(sku);
			tr.insert(name);
			tr.insert(weight);
			
			//ahora añadimos un column por cada bulto
			for(var j=1; j <= numBultos; j++)
			{
				var input = new Element('input', {'type': 'radio', 'name': ps_itemData.productId + '_' + (row+1), 'value':j, 'class':'packedshipment_input', 'onclick': '_recalculateTotals()'});
				if(checkedIndex == j)
					input.writeAttribute('checked', 'checked');
    
				
				var td = new Element('td', {'align':'center', 'valign':'middle', 'class':'packedshipment_added', 'style':'padding:5px;'}).update(input);
				tr.insert(td);
			}
			checkedIndex = (checkedIndex == numBultos) ? 1 : checkedIndex + 1;
			
			
			var oddEven = row%2 ? 'odd' : 'even';
			var tbody = new Element('tbody', {'class':oddEven + ' packedshipment_added'});
			tbody.update(tr);
			

			$('packages_table_modal_1').insert(tbody);
			
			row++;
		} // for
	});
	
	// Añadimos unas filas para los totales que calcula la función recalculateTotals

	// Totales de peso
	var weightTitle = new Element('td', {'align':'right', 'valign':'middle', 'class':'packedshipment_added', 'style':'padding:5px;', 'colspan' : 2});
	weightTitle.update('<span class="headings">Total Weight</span>');
	var totalWeightAll = new Element('td', {'align':'center', 'valign':'middle', 'class':'packedshipment_added', 'style':'padding:5px;'});
	totalWeightAll.update( _getTotalWeightAllBultos().toFixed(4) );
	var weightTotalsRow = new Element('tr', {'class':'border packages_modal_1_product_tr'});
	weightTotalsRow.insert(weightTitle);
	weightTotalsRow.insert(totalWeightAll);
	for(var j = 1; j <= numBultos; j++)
	{   
	    var bultoTotalWeightId = 'bulto_total_weight_' + j;
	    var bultoTotalWeight = new Element('td', {'id': bultoTotalWeightId, 'align':'center', 'valign':'middle', 'class':'packedshipment_added', 'style':'padding:5px;'});
	    weightTotalsRow.insert(bultoTotalWeight);
	}	
	var oddEven = row%2 ? 'odd' : 'even';
	var tbody = new Element('tbody', {'class': oddEven + ' packedshipment_added'});
	tbody.update(weightTotalsRow);	
	$('packages_table_modal_1').insert(tbody);
        
        modalDialog1 = null;
	_recalculateTotals();
	
	row++;
	

		
}


		



function openModal1()
{

	_prepareModal1();
	
	var contenido = $('packages_modal_1').innerHTML;
	
	modalDialog1 = Dialog.confirm(
			contenido,  
			{
				className:"magento",
				//parent:            ps_edit_form,
			    title:             i4PSVars.modalDialog1Title,
				minWidth:          600, 
				minHeight:         300,
				width:             960,
			    height:            400,
				resizable:         true,
				closable:          true,
				minimizable:       false,
				maximizable:       false,
				draggable:         true,
				width:400, 
				okLabel: i4PSVars.continueLabel, 
				cancelLabel: i4PSVars.cancelLabel,
				onOk:_onModal1Ok,
				onCancel: _cancelModales,
				onClose: _cancelModales, 
                                buttonClass: "scalable"
			});
                        
                        
	
}

function _onModal1Ok(win)
{
	
	//por cada bulto creo un objeto en ps_data
	//luego poblaremos ese objeto con los datos correspondientes
	ps_data = [];
	for(var i=0; i< ps_bultosInput.value ; i++)
	{
		ps_data[i] = {'package':i+1, 'ids':[], 'ref':''};
	}
	
	//guardamos los datos introducidos en una variable que luego
	//utilizaremos para inyectar campos hidden en el form y
	//de ahí a submit
	$$("#" + win.getId() + " .packedshipment_input").each(function(el){
		if(el.checked)
		{
			var bultoIndex = el.getValue();
			var tmp = el.readAttribute('name').split("_");
			var productoId = tmp[0];
			ps_data[(bultoIndex - 1)].ids.push(productoId);
			
			//y vamos autocalculando el ref
			//que sera el sku (el primer hijo del tr que esta 2 niveles más arriba)
			ps_data[(bultoIndex - 1)].ref += el.up().up().firstDescendant().innerHTML + " ";
		}	
	})
	
	this.close();
	//_doEditFormSubmit();
	openModal2();
	return false;
}


function _prepareModal2()
{
	//el numero de bultos en la modal 2 ya dependera de los 
	//paquetes que tengan productos despues de modal 1
	
	ps_data.each(function(el){
		if(el.ids.length > 0)
		{
			//anadimos un campo con referencias de los productos que van en el
			var input = new Element('textarea', {
				'id':'package_' + el.package + '_ref',
				'style':"height:6em; width:99%;",
				'cols':'5',
				'rows':'3',
				'name':'packages[' + el.package + '][ref]'
			});
			input.update(el.ref);
			
			var label = new Element('label', {
				'for':'package_' + el.package + '_ref',
				'class':'bold',
			});
                        var referenciaBultoLabel = i4PSVars.useDescriptionsInsteadOfReferences ? i4PSVars.bultoDescriptionLabel : i4PSVars.referenciaBultoLabel;
			label.update(referenciaBultoLabel + el.package);
			
			var span = new Element('span', {
				'class':'field-row packedshipment_added',
			});
			span.insert(label);
			span.insert(input);
			
			$("packages_modal_2_input_container").insert(span);
		}	
	});
	
}



function openModal2()
{
	
	_prepareModal2();
	
	var contenido = $('packages_modal_2').innerHTML;
	
	Dialog.confirm(
			contenido, 
			{
				className:"magento",
				//parent:            ps_edit_form,
			    title:             i4PSVars.modalDialog1Title,
				minWidth:          600, 
				minHeight:         300,
				width:             960,
			    height:            400,
				resizable:         true,
				closable:          true,
				minimizable:       false,
				maximizable:       false,
				draggable:         true,
				width:400, 
				okLabel: i4PSVars.submitLabel, 
				cancelLabel: i4PSVars.cancelLabel,
				onOk:_onModal2Ok,
				onCancel: _cancelModales,
				onClose: _cancelModales,
                                buttonClass : "scalable"
			});

}


function _onModal2Ok(win)
{
	
	//guardamos los datos introducidos en una variable que luego
	//utilizaremos para inyectar campos hidden en el form y
	//de ahí a submit
	$$("#" + win.getId() + " textarea").each(function(el){
		
		var tmp = el.readAttribute('id');
		tmp = tmp.replace("package_", "");
		var bultoIndex = tmp.replace("_ref", "");
		ps_data[(bultoIndex - 1)].ref = el.getValue();
		
	});
	
	_doEditFormSubmit();	
	this.close();	
	return false;
}

function _addressValidationModalOk()
{
    var correctedCityInput = $('packagedshipment[corrected_city]');
    var correctedCity = '';
    if (correctedCityInput)
    {
	if (correctedCityInput.tagName == 'SELECT')
	{
	    correctedCity = correctedCityInput.options[correctedCityInput.selectedIndex].value;
	}
	else 
	{
	    correctedCity = correctedCityInput.value;
	}
	i4ShippingAddressCity = correctedCity;
    }
    
    var correctedPostcodeInput = $('packagedshipment[corrected_postcode]');
    var correctedPostcode = '';
    if (correctedPostcodeInput)
    {
	if (correctedPostcodeInput.tagName == 'SELECT')
	{
	    correctedPostcode = correctedPostcodeInput.options[correctedPostcodeInput.selectedIndex].value;
	}
	else 
	{
	    correctedPostcode = correctedPostcodeInput.value;
	    
	}
	i4ShippingAddressPostcode = correctedPostcode;
	
    }
    
    var dontCorrectAddressInput = $('packagedshipment[dont_correct_address]');
    if (dontCorrectAddressInput && dontCorrectAddressInput.checked)
    {
        i4DontCorrectAddress = true;
    }
    else 
    {
         i4DontCorrectAddress = false;
    }

    this.close();
    
    openAddressValidationModal();
}

function openAddressValidationModal()
{
    
    new Ajax.Request(i4ValidateAddressUrl, {
	method: 'post',
	parameters: {order: i4OrderId, city: i4ShippingAddressCity, postcode: i4ShippingAddressPostcode, countryid: i4ShippingAddressCountryId, dontcorrectaddress: i4DontCorrectAddress ? 1 : 0},
	onSuccess: function(transport){
	    var jsonResponse = transport.responseJSON;
	    if (jsonResponse.dialogHtml)
	    {
		Dialog.confirm(
		    jsonResponse.dialogHtml,  
		    {
			className:"magento",
			title:             "Validación de la dirección del envío",
			minWidth:          600, 
			minHeight:         300,
			width:             960,
			height:            400,
			resizable:         true,
			closable:          true,
			minimizable:       false,
			maximizable:       false,
			draggable:         true,
			width:		    400, 
			okLabel: "continuar", 
			cancelLabel: "cancelar",
			onOk: _addressValidationModalOk,
			onCancel: _cancelModales,
			onClose: _cancelModales,
                        buttonClass : "scalable"
		    });
	    }
	    else 
	    {
		
		if ($('packagedshipment[city]'))
		{
		    var cityParentNode = $('packagedshipment[city]').parentNode;
		    cityParentNode.removeChild($('packagedshipment[city]'));
		}
		if ($('packagedshipment[postcode]'))
		{
		    var postcodeParentNode = $('packagedshipment[postcode]').parentNode;
		    postcodeParentNode.removeChild($('packagedshipment[postcode]'));
		}
		$("packages_modal_2_input_container").insert('<input type="hidden" value="'+i4ShippingAddressCity+
		    '" name="packagedshipment[city]" id="packagedshipment[city]" /> '+
		    '<input type="hidden" value="'+i4ShippingAddressPostcode+
		    '" name="packagedshipment[postcode]" id="packagedshipment[postcode]" /> ');
		openModal1();
	    }
	}
    });  
    
}

function openPackedhipmentModal()
{
    // Si el usuaro ha indicado que quiere que cominiquemos con el servicio de mensajero,
    // abrimos el diálogo para eligir la distribución entre bultos.
    
    if (shouldShowPackedShipmentDialog()) 
    {
 	if (i4IsAddressValidationAvailable) // Este variable se define en i4packedshipment/../address_validation_info_js.phtml
	{
	    openAddressValidationModal();
	}
	else
	{
            openModal1();
	}       
    }
    else 
    {
        ps_edit_form.submit();
    }
}

function shouldShowPackedShipmentDialog() {
    if (communicateShipmentCheckbox && !communicateShipmentCheckbox.checked) { // Este variable se define en i4packedshipment/../items.phtml
        return false;
    }
    
    if (showPackedShipmentDialogCheckbox && !showPackedShipmentDialogCheckbox.checked) { // Este variable se define en i4packedshipment/../items.phtml
        return false;
    }
    
    return true;
}

//cogemos el valor total de items que en principio sera el numero total de bultos que se pueden enviar
Event.observe(window, 'load', function() {
    if ($('packages_modal_1')) {
	initNumBultos();
	
        Event.observe(ps_edit_form, 'packedshipment:submit', function(event){
		    openPackedhipmentModal();
	    });
    } else {
        Event.observe($('edit_form'), 'packedshipment:submit', function(event){
		    $('edit_form').submit();
	    });        
    }

});

