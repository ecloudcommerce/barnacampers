<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Brands
 */


class Amasty_Brands_Block_Catalog_Layer_View extends Amasty_Brands_Block_Catalog_Layer_View_Pure
{
    /**
     * Get all fiterable attributes of current category
     *
     * @return array
     */
    protected function _getFilterableAttributes()
    {
        /** @var Mage_Catalog_Model_Resource_Product_Attribute_Collection $attributes */
        $attributes = parent::_getFilterableAttributes();
        if (!$attributes || $this->getData('_ambrands_processed')) {
            return $attributes;
        }
        $brandId = Mage::app()->getRequest()->getParam('ambrand_id');
        if (!Mage::getStoreConfig('ambrands/general/attribute_filter') || $brandId) {
            $brandCode = Mage::helper('ambrands')->getBrandAttributeCode();
            $attributes
                ->clear()
                ->setPageSize(false)
                ->addFieldToFilter('attribute_code', array('neq' => $brandCode))
                ->load();
        }
        $this->setData('_filterable_attributes', $attributes);
        $this->setData('_ambrands_processed', 1);
        return $attributes;
    }

    public function _toHtml()
    {
        if ('true' == (string)Mage::getConfig()->getNode('modules/Amasty_Shopby/active')) {
            return '';
        }
        return parent::_toHtml();
    }
}
