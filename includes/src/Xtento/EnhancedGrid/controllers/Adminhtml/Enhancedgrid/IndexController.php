<?php

/**
 * Product:       Xtento_EnhancedGrid (1.7.0)
 * ID:            zc/SV5tEtFi8UKC8r8I8FNXX2qtQJB3WgaciIXnJygw=
 * Packaged:      2015-07-13T16:05:27+00:00
 * Last Modified: 2013-11-25T17:50:54+01:00
 * File:          app/code/local/Xtento/EnhancedGrid/controllers/Adminhtml/Enhancedgrid/IndexController.php
 * Copyright:     Copyright (c) 2015 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_EnhancedGrid_Adminhtml_Enhancedgrid_IndexController extends Mage_Adminhtml_Controller_Action
{
    public function permissionsAction()
    {
        Mage::getSingleton('adminhtml/session')->addWarning(Mage::helper('xtento_enhancedgrid')->__('You don\'t have rights to customize grids. Please go to System > Permissions > Roles and assign the "XTENTO Enhanced Grid > Grid Customization" permission to your admin role.'));
        $this->loadLayout();
        $this->renderLayout();
    }
}