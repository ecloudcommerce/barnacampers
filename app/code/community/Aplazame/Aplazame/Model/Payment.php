<?php

class Aplazame_Aplazame_Model_Payment extends Mage_Payment_Model_Method_Abstract
{
    const METHOD_CODE = 'aplazame';

    protected $_code = self::METHOD_CODE;
    protected $_formBlockType = 'aplazame/payment_form';
    protected $_infoBlockType = 'aplazame/payment_info';
    protected $_canUseInternal = false;
    protected $_canUseForMultishipping = false;
    protected $_canRefund = true;
    protected $_canRefundInvoicePartial = true;
    protected $_canVoid = true;

    /**
     * @param Mage_Sales_Model_Order_Payment $payment
     * @param float $amount
     *
     * @return $this
     */
    public function authorize(Varien_Object $payment, $amount)
    {
        if ($payment->getIsFraudDetected()) {
            return $this;
        }

        $this->getPaymentsHelper()->authorizePayment($payment);

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order_Payment $payment
     *
     * @return bool
     */
    public function acceptPayment(Mage_Payment_Model_Info $payment)
    {
        return $this->getPaymentsHelper()->isPaymentAccepted($payment, $this->getConfigData('autoinvoice'));
    }

    /**
     * @param Mage_Sales_Model_Order_Payment $payment
     *
     * @return bool
     */
    public function denyPayment(Mage_Payment_Model_Info $payment)
    {
        return true;
    }

    /**
     * @param Mage_Sales_Model_Order_Payment $payment
     *
     * @return $this
     */
    public function cancel(Varien_Object $payment)
    {
        $order = $payment->getOrder();

        try {
            $this->getPaymentsHelper()->getApiClient()->cancelOrder($order);
        } catch (Aplazame_Sdk_Api_ApiClientException $e) {
            if ($e->getStatusCode() == 404) {
                return $this;
            }

            throw $e;
        }

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order_Payment $payment
     *
     * @return $this
     */
    public function void(Varien_Object $payment)
    {
        return $this->cancel($payment);
    }

    /**
     * @param Mage_Sales_Model_Order_Payment $payment
     * @param float $amount
     *
     * @return $this
     */
    public function refund(Varien_Object $payment, $amount)
    {
        $order = $payment->getOrder();

        try {
            $this->getPaymentsHelper()->getApiClient()->refundAmount($order, $amount);
        } catch (Aplazame_Sdk_Api_ApiClientException $e) {
            throw $e;
        }

        return $this;
    }

    public function isMagentoNativeRefundMethodEnable()
    {
        return (bool) $this->getConfigData('refund_method_magento_native');
    }

    /**
     * Get checkout session namespace
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    public function getOrderPlaceRedirectUrl()
    {
        return $this->getPaymentsHelper()->getAplazameRedirectUrl('instalments');
    }

    /**
     * @return array
     *
     * @throws Aplazame_Sdk_Api_AplazameExceptionInterface
     */
    public function createCheckoutOnAplazame()
    {
        return $this->getPaymentsHelper()->getAplazameCheckout($this->getCheckout()->getLastRealOrderId(), 'instalments');
    }

    public function getCheckoutSerializer()
    {
        return $this->getPaymentsHelper()->getAplazameCheckout($this->getCheckout()->getLastRealOrderId(), 'instalments', true);
    }

    /**
     * @return Aplazame_Aplazame_Helper_Payments
     */
    private function getPaymentsHelper()
    {
        return Mage::helper('aplazame/payments');
    }
}
