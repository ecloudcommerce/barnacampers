<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2014 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Helper_Ecommerce extends Mage_Core_Helper_Abstract {
 
    
    protected $_brandAttributeCode = null;


   public function getBrandAttributeCode($store = null) {
       if(is_null($this->_brandAttributeCode)) {
           $this->_brandAttributeCode = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/brand', $store);
       }
       return $this->_brandAttributeCode;
   }

   public function getProductAttributeValue($product, $attributeCode) {
       if(!$product || !$product->getId()) {
           return '';
       }
       $attribute = Mage::helper('wcooall/attribute')->getAttribute($attributeCode);
       if(!$attribute->getId()) {
           return '';
       }
       if($product->getData($attributeCode)) {
           $data = $product->getData($attributeCode);
       } else {
            $data = Mage::getSingleton('catalog/product')->getResource()->getAttributeRawValue($product->getId(), $attributeCode, Mage::app()->getStore());
       }
       if(!$data) {
           return '';
       }
       if ($attribute->usesSource()) {
            $data = $attribute->getSource()->getOptionText($data);
            if(is_array($data)) {
                $data = implode(', ', $data);
            }
       }
       return $data;
   }

   public function getProductCategoryValue($product, $useCurrentCategory = true) {
       if(!$product || !$product->getId()) {
           return '';
       }
       $categoryId = false;
       if(Mage::helper('wcooall')->isModuleInstalled('Webcooking_MainCategory')) {
           $categoryId = $product->getMainCategory();
       }
       if(!$categoryId) {
           $category = Mage::registry('current_category');
           if($useCurrentCategory && $category && $category->getId()) {
               $categoryId = $category->getId();
           } else {
                /*$categoryIds = $product->getCategoryIds();
                $categoryId = array_pop($categoryIds);*/
                $categoryCollection = $product->getCategoryCollection();
                foreach($categoryCollection as $category) {
                    if($category->getIsActive() && $category->getLevel() > 1) {
                        $categoryId = $category->getId();
                    }
                }
           }
       }
       if(!$categoryId) {
           return '';
       }
       return $this->formatData(Mage::helper('wcooall/category')->getCategoryNameById($categoryId, '/', 5));
   }

   public function formatData($data) {
       $data = str_replace('"', '', $data);
       $data = str_replace("'", '', $data);
       return Mage::helper('wcooall')->applyReplaceAccent($data);
   }

   public function getDefaultListName($useCategoryName = false) {
       $listName =  Mage::app()->getFrontController()->getAction()->getFullActionName();
       switch($listName) {
           case 'cms_index_index':
               $listName = $this->__('Homepage');
               break;
           case 'catalog_category_view':
               $category = Mage::registry('current_category');
               if($category && $category->getId() && $useCategoryName) {
                   $listName = $this->__('Category %s', $category->getName());
               } else {
                   $listName = $this->__('Category view');
               }
               break;
           case 'promotions_promotions_index':
               $listName = $this->__('Promotions');
               break;
           case 'solrsearch_result_index':
           case 'catalogsearch_result_index':
               $listName = $this->__('Search results');
               break;
           case 'advancedcms_page_view':
           case 'cms_page_view':
               $listName = $this->__('CMS page view');
               break;
           case 'nouveautes-new-index':
               $listName = $this->__('Recent products');
               break;
       }
       return $listName;
   }

   public function getAddPromoImpressionTag($id, $name='', $creative='', $position=false, $withScriptTags = true) {
       if(!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
           return '';
       }
       $html = '';
       if($withScriptTags) {
           $html .= '<script type="text/javascript">';
       }

       $html .= sprintf("
                    ga('ec:addPromo', {
                        'id': '%s',
                        'name': '%s',
                        'creative': '%s',
                        'position': '%s'
                    });
                    ",
                    $this->formatData($id),
                    $this->formatData($name),
                    $this->formatData($creative),
                    $this->formatData($position)
                );


       if($withScriptTags) {
           $html .= '</script>';
       }
       return $html;

   }

   public function getAddProductImpressionTag($product, $listName=false, $position = false, $withScriptTags = true) {
       if(!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated() || !$product || !$product->getId()) {
           return '';
       }
       //TO remove. Temporary solution to avoid exceeding payload max size.
       if($position > 20) {
           return '';
       }
       $html = '';
       if($withScriptTags) {
           $html .= '<script type="text/javascript">';
       }

       $currency = Mage::app()->getStore()->getCurrentCurrencyCode();
       $brandValue = $this->getProductBrandValue($product);
       $nameValue = $this->getProductNameValue($product);
       $variantValue = $this->getProductVariantValue($product);
       $skuValue = $this->getProductSkuValue($product);
       $categoryValue = $this->getProductCategoryValue($product);

       if(!$listName) {
           $listName = $this->getDefaultListName();
       }

       if(Mage::helper('googleuniversalanalytics')->isTagManagerModeActivated()) {
           //deprecated
            $html .= sprintf("
                         dataLayer.push({
                            'ecommerce': {
                              'currencyCode': '%s',                       
                              'impressions': [
                               {    
                                 'id': '%s',
                                 'name': '%s', 
                                 'category': '%s',
                                 'brand': '%s',
                                 'variant': '%s',
                                 'price': '%s',
                                 'list': '%s',
                                 'position': '%s'
                               }
                               ]
                            }
                          });
                         ",
                         $currency,
                         $skuValue,
                         $nameValue,
                         $categoryValue,
                         $brandValue,
                         $variantValue,
                         $product->getFinalPrice(),
                         $this->formatData($listName),
                         intval($position)
                     );
       } else {
           $html .= sprintf("
                     ga('ec:addImpression', {
                         'id': '%s',
                         'name': '%s',
                         'category': '%s',
                         'brand': '%s',
                         'variant': '%s',
                         'price': '%s',
                         'list': '%s',
                         'position': '%s'
                     });
                     ",
                     $skuValue,
                     $nameValue,
                     $categoryValue,
                     $brandValue,
                     $variantValue,
                     $product->getFinalPrice(),
                     $this->formatData($listName),
                     intval($position)
                 );
       }


       if($withScriptTags) {
           $html .= '</script>';
       }
       return $html;
   }
   
   
   
   public function getAddProductDetailsTag($product, $withScriptTags = false) {
       if(!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
            return '';
        }
        if(!$product) {
            return '';
        }
        
        $html = '';
        
        if($withScriptTags) {
            $html = '<script>';
        } 
        
        if(Mage::helper('googleuniversalanalytics')->isTagManagerModeActivated()) {
            //deprecated
            $html .= sprintf("
                         dataLayer.push({
                            'ecommerce': {
                                'detail': {
                                  'products': [{
                                    'name': '%s',         
                                    'id': '%s',
                                    'price': '%s',
                                    'brand': '%s',
                                    'category': '%s',
                                    'variant': '%s'
                                   }]
                                 }
                                }
                          });
                         ",
                         $this->getProductNameValue($product),
                         $this->getProductSkuValue($product),
                         $product->getFinalPrice(),
                         $this->getProductBrandValue($product),
                         $this->getProductCategoryValue($product),
                         ''
                     );
       } else {
           if($product->getTypeId() == 'configurable' && Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/track_variants')) {
                $html .= 'Product.Config.prototype.configure = function(event){
                                var element = Event.element(event);
                                this.configureElement(element);
                                sendGuaProductVariantDetails(this);
                            };';
            }
            $html .= sprintf("
                var productDetail = {
                    'id': '%s',
                    'name': '%s',
                    'category': '%s',
                    'brand': '%s',
                    'price': '%s',
                    'variant': '%s'
                };
                ga('ec:addProduct', productDetail);
                ga('ec:setAction', 'detail');
                ",
                $this->getProductSkuValue($product),
                $this->getProductNameValue($product),
                $this->getProductCategoryValue($product), 
                $this->getProductBrandValue($product), 
                $product->getFinalPrice(),
                ''
            );
       }
        
        if($withScriptTags) {
           $html .= '</script>'; 
        }
        
        return $html;
   }



   public function getProductBrandValue($product) {
       $brandAttributeCode = $this->getBrandAttributeCode();
       $brandValue = $this->formatData($this->getProductAttributeValue($product, $brandAttributeCode));
        
       $brandObj = new Varien_Object(array('value' => $brandValue));
       Mage::dispatchEvent('gua_get_product_brand_value', array('brand' => $brandObj, 'product'=>$product));
       $brandValue = $brandObj->getValue();
       
       return $brandValue;
   }

   public function getProductNameValue($product) {
       $nameValue = $this->formatData($this->getProductAttributeValue($product, 'name'));
       return $nameValue;
   }

   public function getProductVariantValue($product, $orderItem = null) {
       if (!$product || $product->getTypeId() != 'configurable') {
            return '';
        }
        try {
            if ($orderItem && get_class($orderItem) == 'Mage_Sales_Model_Order_Item') {
                $options = array();
                if ($productOptions = $orderItem->getProductOptions()) {
                    if (isset($productOptions['options'])) {
                        $options = array_merge($options, $productOptions['options']);
                    }
                    if (isset($productOptions['additional_options'])) {
                        $options = array_merge($options, $productOptions['additional_options']);
                    }
                    if (!empty($productOptions['attributes_info'])) {
                        $options = array_merge($productOptions['attributes_info'], $options);
                    }
                }
            } else {

                $options = $product->getTypeInstance(true)->getSelectedAttributesInfo($product);
            }
        } catch (Exception $e) {
            return '';
        }
        $variant = array();
        foreach ($options as $option) {
            $variant[] = $option['value'];
        }
        return implode(' - ', $variant);
    }

   public function getProductSkuValue($product) {
       $skuValue = $this->formatData($product->getSku());
       return $skuValue;
   }

   public function getProductLinkData($product, $listName=false, $position = false) {
       if(!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated() || !$product || !$product->getId()) {
           return '';
       }
       $html = ' ';

       $brandValue = $this->getProductBrandValue($product);
       $nameValue = $this->getProductNameValue($product);
       $variantValue = $this->getProductVariantValue($product);
       $skuValue = $this->getProductSkuValue($product);
       $categoryValue = $this->getProductCategoryValue($product);

       if(!$listName) {
           $listName = $this->getDefaultListName();
       }

       $html .= 'data-gua-ec-id="' . $skuValue . '" ';
       $html .= 'data-gua-ec-name="' . $nameValue . '" ';
       $html .= 'data-gua-ec-category="' . $categoryValue . '" ';
       $html .= 'data-gua-ec-brand="' . $brandValue . '" ';
       $html .= 'data-gua-ec-variant="' . $variantValue . '" ';
       $html .= 'data-gua-ec-list="' . $this->formatData($listName) . '" ';
       $html .= 'data-gua-ec-price="' . $product->getFinalPrice() . '" ';
       $html .= 'data-gua-ec-position="' . intval($position) . '" ';


       return $html;
   }



   public function getPromoLinkData($id, $name='', $creative='', $position=false) {
       if(!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated() ) {
           return '';
       }
       $html = ' ';

       $html .= 'data-gua-ec-promo-id="' . $this->formatData($id) . '" ';
       $html .= 'data-gua-ec-promo-name="' . $this->formatData($name) . '" ';
       $html .= 'data-gua-ec-promo-creative="' . $this->formatData($creative) . '" ';
       $html .= 'data-gua-ec-promo-position="' . $this->formatData($position) . '" ';


       return $html;
   }

   public function getEnhancedOrdersTrackingCode($orderIds) {
        if(Mage::getStoreConfig('googleuniversalanalytics/mp_transactions/ecommerce')) {
            return '';
        }
        if (empty($orderIds) || !is_array($orderIds)) {
            return '';
        }
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('entity_id', array('in' => $orderIds))
        ;
        $result = array("ga('require', 'ecommerce', 'ecommerce.js');");
        foreach ($collection as $order) {
            if ($order->getIsVirtual()) {
                $address = $order->getBillingAddress();
            } else {
                $address = $order->getShippingAddress();
            }


            foreach ($order->getAllVisibleItems() as $item) {
                $result[] = sprintf("
                    ga('ec:addProduct', {
                        'id': '%s',
                        'name': '%s',
                        'category': '%s',
                        'brand': '%s',
                        'variant': '%s',
                        'price': '%s',
                        'quantity': %s
                    });
                    ",
                    $this->jsQuoteEscape($item->getSku()),
                    $this->jsQuoteEscape(Mage::helper('wcooall')->applyReplaceAccent($item->getName())),
                    $this->jsQuoteEscape($this->getProductCategoryValue($item->getProduct())),
                    $this->jsQuoteEscape($this->getProductBrandValue($item->getProduct())),
                    $this->jsQuoteEscape($this->getProductVariantValue($item->getProduct(), $item)),
                    Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item),
                    round($item->getQtyOrdered())
                );
            }


               $result[] = sprintf("
                 ga('ec:setAction', 'purchase', {
                    'id': '%s',
                    'affiliation': '%s',
                    'revenue': '%s',
                    'tax': '%s',
                    'shipping': '%s',
                    'coupon': '%s'
                  });
                 ",
                $order->getIncrementId(),
                null,
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order),
                $order->getCouponCode()
            );


        }
        return implode("\n", $result);
    }
    
    
    public function getTransactionRevenueForOrder($order) {
        $storeId = $order->getStoreId();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/mp_transactions/transaction_revenue_inc_tax', $storeId) && Mage::getStoreConfigFlag('googleuniversalanalytics/mp_transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $order->getGrandTotal();
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/mp_transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $order->getGrandTotal() - $order->getTaxAmount();
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/mp_transactions/transaction_revenue_inc_tax', $storeId)) {
            return $order->getGrandTotal() - $order->getShippingAmount();
        } 
        return $order->getGrandTotal() - $order->getTaxAmount() - $order->getShippingAmount();
    }
    
    
    public function getTransactionRevenueForInvoice($invoice) {
        $storeId = $invoice->getOrder()->getStoreId();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/mp_transactions/transaction_revenue_inc_tax', $storeId) && Mage::getStoreConfigFlag('googleuniversalanalytics/mp_transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $invoice->getGrandTotal();
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/mp_transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $invoice->getGrandTotal() - $invoice->getTaxAmount();
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/mp_transactions/transaction_revenue_inc_tax', $storeId)) {
            return $invoice->getGrandTotal() - $invoice->getShippingAmount();
        } 
        return $invoice->getGrandTotal() - $invoice->getTaxAmount() - $invoice->getShippingAmount();
    }
    
    public function getTransactionShippingForOrder($order) {
        $storeId = $order->getStoreId();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/mp_transactions/transaction_shipping_inc_tax', $storeId)) {
           return $order->getShippingAmount() + $order->getShippingTaxAmount();
        } 
        return $order->getShippingAmount();
    }
    
    public function getTransactionShippingForInvoice($invoice) {
        $storeId = $invoice->getOrder()->getStoreId();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/mp_transactions/transaction_shipping_inc_tax', $storeId)) {
           return $invoice->getShippingAmount() + $invoice->getShippingTaxAmount();
        } 
        return $invoice->getShippingAmount();
    }
    
    public function getTransactionTaxForOrder($order) {
        $storeId = $order->getStoreId();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/mp_transactions/transaction_tax_inc_shipping_tax', $storeId)) {
            return $order->getTaxAmount();
        } 
        return $order->getTaxAmount() - $order->getShippingTaxAmount();
    }
    
    public function getTransactionTaxForInvoice($invoice) {
        $storeId = $invoice->getOrder()->getStoreId();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/mp_transactions/transaction_tax_inc_shipping_tax', $storeId)) {
            return $invoice->getTaxAmount();
        } 
        return $invoice->getTaxAmount() - $invoice->getShippingTaxAmount();
    }
    
    public function getItemPriceForOrder($orderItem) {
        $order = $orderItem->getOrder();
        $storeId = null;
        if($order) {
            $storeId = $order->getStoreId();
        }
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/mp_transactions/transaction_item_price_inc_tax', $storeId)) {
            return $orderItem->getPriceInclTax()?$orderItem->getPriceInclTax():$orderItem->getPrice();
        } 
        return $orderItem->getPrice();
    }
    
    public function getItemPriceForInvoice($invoiceItem) {
        $storeId = $invoiceItem->getInvoice()->getOrder()->getStoreId();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/mp_transactions/transaction_item_price_inc_tax', $storeId)) {
            return $invoiceItem->getPriceInclTax()?$invoiceItem->getPriceInclTax():$invoiceItem->getPrice();
        } 
        return $invoiceItem->getPrice();
    }

}