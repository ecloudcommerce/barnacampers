<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2014 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Block_Gua extends Mage_Core_Block_Template
{
    
    protected function _construct()
    {
        parent::_construct();
        $this->setCacheLifetime(null);
    }


    public function getClientId() {
        return Mage::helper('googleuniversalanalytics')->getClientId(false);
    }
    
    public function getPageName()
    {
        return $this->_getData('page_name');
    }

    
    protected function _getInitTrackingCode($accountId)
    {
        $guaOptions = Mage::helper('googleuniversalanalytics')->getGaCreateOptions();
        $guaCode  = "ga('create', '{$this->jsQuoteEscape($accountId)}', 'auto', ".$guaOptions.");" . "\n";
        
        if(Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
            $guaCode .= "ga('require', 'ec');" . "\n";
            $guaCode .= "ga('set', '&cu', '" . Mage::app()->getStore()->getCurrentCurrencyCode() . "');" . "\n"; 
        }
        
        if(Mage::helper('googleuniversalanalytics')->isDemographicsActivated()) {
            $guaCode .= "ga('require', 'displayfeatures');" . "\n";
        }
        
        if(Mage::helper('googleuniversalanalytics')->isForceSSLActivated()) {
            $guaCode .= "ga('set', 'forceSSL', true);" . "\n";
        }
        
        if(Mage::helper('googleuniversalanalytics')->isEnhancedLinkActivated()) {
            $guaCode .= "ga('require', 'linkid');   // Load the plug-in. Note: this call will block until the plug-in is loaded." . "\n";
        }
        
        if(Mage::helper('googleuniversalanalytics')->isLinkerActivated()) {
            $guaCode .= "ga('require', 'linker');" . "\n";
            $guaCode .= "ga('linker:autoLink', ".Mage::helper('googleuniversalanalytics')->getLinkerAutoLink().", " . (Mage::helper('googleuniversalanalytics')->isLinkerParametersInAnchor()?'true':'false') . ", " . (Mage::helper('googleuniversalanalytics')->isLinkerCrossDomainAutoLinkingForForms()?'true':'false') . ");" . "\n";
        }
        
        if(Mage::helper('googleuniversalanalytics')->isIPAnonymized()) {
            $guaCode .= "ga('set', 'anonymizeIp', true);" . "\n";
        }
        
        return $guaCode;
    }
    
    
    protected function _getOptOutTrackingCode($accountId)
    {
        $guaOptOutCode = '';
        if(Mage::helper('googleuniversalanalytics')->isOptOutActivated()) {
            $guaOptOutCode = "var disableStr = 'ga-disable-{$accountId}';" . "\n";

            $guaOptOutCode .= "if (document.cookie.indexOf(disableStr + '=true') > -1) {" . "\n";
            $guaOptOutCode .= "  window[disableStr] = true;" . "\n";
            $guaOptOutCode .= "}" . "\n";
            $guaOptOutCode .= "\n";

            $guaOptOutCode .= "function gaOptout() {" . "\n";
            $guaOptOutCode .= "  document.cookie = disableStr + '=true; expires=Thu, 31 Dec ".(date('Y')+10)." 23:59:59 UTC; path=/';" . "\n";
            $guaOptOutCode .= "  window[disableStr] = true;" . "\n";
            $guaOptOutCode .= "}" . "\n";
        }
        
        return $guaOptOutCode;
    }

    protected function _getPageViewTrackingCode($accountId)
    {
        $pageName   = trim($this->getPageName());
        $optPageURL = '';
        if ($pageName && preg_match('/^\/.*/i', $pageName)) {
            $optPageURL = ", '{$this->jsQuoteEscape($pageName)}'";
        }
        
        $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        $guaCode = "ga('{$trackerName}send', 'pageview'{$optPageURL});" . "\n";
        return $guaCode;
    }
    
    
    
    protected function _getOrdersTrackingCode()
    {
        if(Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
                return Mage::helper('googleuniversalanalytics/ecommerce')->getEnhancedOrdersTrackingCode($this->getOrderIds());
        }
        if(Mage::getStoreConfig('googleuniversalanalytics/mp_transactions/ecommerce')) {
            return '';
        }
        $orderIds = $this->getOrderIds();
        if (empty($orderIds) || !is_array($orderIds)) {
            return '';
        }
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('entity_id', array('in' => $orderIds))
        ;
        $result = array("ga('require', 'ecommerce', 'ecommerce.js');");
        foreach ($collection as $order) {
            if ($order->getIsVirtual()) {
                $address = $order->getBillingAddress();
            } else {
                $address = $order->getShippingAddress();
            }
            
            
               $result[] = sprintf("
                 ga('ecommerce:addTransaction', {
                    'id': '%s',                     
                    'affiliation': '%s',   
                    'revenue': '%s',               
                    'shipping': '%s',                  
                    'tax': '%s'                     
                  });  
                 ",
                $order->getIncrementId(),
                null,
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order)
            );
            
            foreach ($order->getAllVisibleItems() as $item) {
                $result[] = sprintf("
                    ga('ecommerce:addItem', {
                        'id': '%s',                     
                        'name': '%s',    
                        'sku': '%s',                 
                        'category': '%s',         
                        'price': '%s',                 
                        'quantity': '%s'                   
                      });
                    ",
                    $order->getIncrementId(),
                    $this->jsQuoteEscape(Mage::helper('wcooall')->applyReplaceAccent($item->getName())),
                    $this->jsQuoteEscape($item->getSku()), 
                    null, 
                    Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item), 
                    round($item->getQtyOrdered())
                );
            }
            $result[] = "ga('ecommerce:send');";
        }
        return implode("\n", $result);
    }
    
    
    public function _getProductDetailsViewCode() {
        $product = Mage::registry('current_product');
        return Mage::helper('googleuniversalanalytics/ecommerce')->getAddProductDetailsTag($product);
    }
    
    public function _getCustomDimensionsCode() {
        $guaCode = '';
        $customer = Mage::helper('customer')->getCustomer();
        if(!$customer->getId()) {
            $customer = false;
        }
        $product = Mage::registry('current_product');
        if ($product && ($idx = Mage::getStoreConfig('googleuniversalanalytics/mp_dimensions/product_id'))) {
            $guaCode .= "ga('set','dimension{$idx}','" . $product->getId() . "');" . "\n";
        }
        if ($product && ($idx = Mage::getStoreConfig('googleuniversalanalytics/mp_dimensions/product_sku'))) {
            $guaCode .= "ga('set','dimension{$idx}','" . $product->getSku() . "');" . "\n";
        }
        if ($product && ($idx = Mage::getStoreConfig('googleuniversalanalytics/mp_dimensions/product_name'))) {
            $guaCode .= "ga('set','dimension{$idx}','" . str_replace("'", "\'", $product->getName()) . "');" . "\n";
        }
        if ($customer && ($idx = Mage::getStoreConfig('googleuniversalanalytics/mp_dimensions/customer_id'))) {
            $guaCode .= "ga('set','dimension{$idx}','" . $customer->getId() . "');" . "\n";
        }
        if ($customer && ($idx = Mage::getStoreConfig('googleuniversalanalytics/mp_dimensions/customer_group_id'))) {
            $guaCode .= "ga('set','dimension{$idx}','" . $customer->getGroupId() . "');" . "\n";
        }
        if ($customer && ($idx = Mage::getStoreConfig('googleuniversalanalytics/mp_dimensions/customer_email'))) {
            $guaCode .= "ga('set','dimension{$idx}','" . str_replace("'", "\'", $customer->getEmail()) . "');" . "\n";
        }
        if ($idx = Mage::getStoreConfig('googleuniversalanalytics/mp_dimensions/page_type')) {
            $guaCode .= "ga('set','dimension{$idx}','" . Mage::helper('googleuniversalanalytics/remarketing')->getPageType() . "');" . "\n";
        }
        if ($idx = Mage::getStoreConfig('googleuniversalanalytics/mp_dimensions/total_value') && Mage::helper('googleuniversalanalytics/remarketing')->getTotalValue()) {
            $guaCode .= "ga('set','dimension{$idx}','" . Mage::helper('googleuniversalanalytics/remarketing')->getTotalValue() . "');" . "\n";
        }
        return $guaCode;
    }

    protected function _toHtml()
    {
        if (!Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable()) {
            return '';
        }

        return parent::_toHtml();
    }
    
    
    protected function _getReferrer() {
        if(!Mage::getStoreConfigFlag('googleuniversalanalytics/options/override_referrer')) {
            return false;
        }
        $coreSession = Mage::getSingleton('core/session');
        if(!$coreSession->getGuaHttpReferer()) {
            $coreSession->setGuaHttpReferer(Mage::helper('core/http')->getHttpReferer());
        }
        return $coreSession->getGuaHttpReferer();
    }
    
    
}
