<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Brands
 */

class Amasty_Brands_Model_Shopby_Url_Builder extends Amasty_Shopby_Model_Url_Builder
{
    public function getUrl()
    {
        $url = null;
        $brandId = Mage::app()->getRequest()->getParam('ambrand_id', null);
        $brand = Mage::getModel('ambrands/brand')->load($brandId);
        if ($brand->getId() && $brand->getUrlKey()) {
            $url = $brand->getUrl();
            if ($url) {
                $query = http_build_query($this->query);
                if (strlen($query)) {
                    $url .= '?' . $query;
                }
            }
        }
        if ($url) {
            return $url;
        }
        if (is_callable('parent::getUrl')) {
            return parent::getUrl();
        }
        return Mage::getBaseUrl();
    }
}