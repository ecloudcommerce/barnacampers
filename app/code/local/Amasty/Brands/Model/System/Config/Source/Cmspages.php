<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Brands
 */

class Amasty_Brands_Model_System_Config_Source_Cmspages
{
    public function toOptionArray()
    {
        $cms_arr = Mage::getModel('cms/page')->getCollection()->toOptionArray();
        $cms_arr[''] = "-Select CMS Page-";
        return $cms_arr;
    }
}