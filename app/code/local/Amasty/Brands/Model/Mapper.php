<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Brands
 */


/**
 * Class Amasty_Brands_Model_Mapper.php
 *
 * @author Artem Brunevski
 */
class Amasty_Brands_Model_Mapper extends Varien_Object
{
    /** @var Amasty_Brands_Model_Config  */
    protected $_config;

    /**
     * Amasty_Brands_Model_Mapper constructor.
     */
    public function __construct()
    {
        $this->_config = Mage::getSingleton('ambrands/config');
    }

    public function run()
    {
        /** @var Mage_Eav_Model_Entity_Attribute_Option $brandOption */
        foreach($this->_getUnmappedOptionsCollection() as $brandOption)
        {
            $this->_add($brandOption);
        }
    }

    /**
     * Add brand according to brand option
     * @param Mage_Eav_Model_Entity_Attribute_Option $brandOption
     * @param int $interaction
     * @throws Exception
     * @throws Zend_Db_Statement_Exception
     */
    protected function _add(Mage_Eav_Model_Entity_Attribute_Option $brandOption, $interaction = 0)
    {
        /** @var Amasty_Brands_Model_Brand $brand */
        $brand = Mage::getModel('ambrands/brand');

        try{
            $optionValue = $brandOption->getValue();
            if ($interaction > 0){
                $optionValue .= '-'. $interaction;
            }
            $brand->addData(array(
                'option_id' => $brandOption->getId(),
                'url_key' => $brand->formatUrlKey($optionValue),
                'name' => $brandOption->getValue(),
                'is_active' => true,
            ));

            if ($brand->validate()){
                $this->_processProducts($brand, $brandOption->getId());
                $brand->save();
            }

        } catch (Zend_Db_Statement_Exception $e){
            if ($e->getCode() === 23000 && $interaction < 10){ //duplicate entity
                $this->_add($brandOption, ++$interaction);
            } else {
                throw new Zend_Db_Statement_Exception($e->getMessage(), $e->getCode());
            }
        }
    }

    /**
     * @param Amasty_Brands_Model_Brand $brand
     * @param int $brandOptionId
     * @return $this
     */
    protected function _processProducts($brand, $brandOptionId)
    {
        $attrCode = $this->_getAttribute()->getAttributeCode();

        $attributeProductCollection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToFilter($attrCode, $brandOptionId);
        $attributeProducts = array();
        foreach ($attributeProductCollection as $product) {
            $attributeProducts[$product->getId()] = 0;
        }

        $brandProducts = $brand->getProductsPosition();
        $extraProductIds = array_keys(array_diff_key($brandProducts, $attributeProducts));
        $missedProducts = array_diff_key($attributeProducts, $brandProducts);

        if (count($missedProducts)) {
            $brand->setPostedProducts($brandProducts + $missedProducts);
        }

        if (count($extraProductIds)) {
            $attributeProductCollection
                ->addFieldToFilter('entity_id', array('in' => $extraProductIds));;
            foreach ($attributeProductCollection as $product) {
                $product->setData($attrCode, $brandOptionId);
            }
            $attributeProductCollection->save();
        }
        
        return $this;        
    }

    /**
     * @return Mage_Eav_Model_Resource_Entity_Attribute_Option_Collection
     */
    protected function _getUnmappedOptionsCollection()
    {
        /** @var Mage_Eav_Model_Resource_Entity_Attribute_Option_Collection $collection */

        $collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
            ->setAttributeFilter($this->_getAttribute()->getId())
            ->setStoreFilter($this->_getAttribute()->getStoreId());

        $collection->getSelect()->joinLeft(
            array('ambrands'  => $collection->getTable('ambrands/entity')),
            'ambrands.option_id = main_table.option_id',
            array()
        )->where('ambrands.entity_id is null');

        $collection->load();

        return $collection;
    }

    /**
     * @return Mage_Eav_Model_Entity_Attribute
     */
    protected function _getAttribute()
    {
        return $this->_config->getBrandAttribute();
    }
}