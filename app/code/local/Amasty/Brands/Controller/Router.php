<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Brands
 */

class Amasty_Brands_Controller_Router extends Mage_Core_Controller_Varien_Router_Standard
{
    protected $request;
    
    public function match(Zend_Controller_Request_Http $request)
    {
        $this->request  = $request;
        $brandUrl = $this->request->getPathInfo();
        $brandUrl = $this->_removeBrandUrlKey($brandUrl);
        if(trim($brandUrl,'/') === '')
            return false;

        $brandUrl = $this->_removeSuffix($brandUrl);
        $brandUrl = trim($brandUrl, '/');

        $storeId = Mage::app()->getStore()->getId();
        $brand = Mage::getModel('ambrands/brand')->setStoreId($storeId)->loadByAttribute('url_key', $brandUrl);
        if (!$brand || !$brand->getId()) {
            return false;
        }
        if (!$brand->getIsActive()) {
            return false;
        }
        $this->request->setParam('ambrand_id',$brand->getId());

        $this->forwardAction();
    }

    protected function forwardAction()
    {
        $realModule = 'Amasty_Brands';

        $this->request->setModuleName('ambrands');
        $this->request->setRouteName('ambrands');
        $this->request->setControllerName('index');
        $this->request->setActionName('view');
        $this->request->setControllerModule($realModule);

        $file = Mage::getModuleDir('controllers', $realModule) . DS . 'IndexController.php';
        include $file;

        //compatibility with 1.3
        $class = $realModule . '_IndexController';
        $controllerInstance = new $class($this->request, $this->getFront()->getResponse());

        $this->request->setDispatched(true);
        $controllerInstance->dispatch('view');
    }


    protected function _removeBrandUrlKey($origUrl)
    {

        $brandUrlKey = Mage::helper('ambrands')->getBrandsUrl();
        if(!$brandUrlKey)
            return $origUrl;

        $len = strlen($brandUrlKey);

        $url = $origUrl;
        $url = ltrim($url, '/');

        if(substr($url,0,$len) == $brandUrlKey)
            $origUrl = substr($url, $len);

        return $origUrl;
    }

    protected function _removeSuffix($url)
    {
        $suffix = $this->_getUrlSuffix();
        if ($suffix == '') {
            return $url;
        }

        $l = strlen($suffix);
        if (substr_compare($url, $suffix, -$l) == 0) {
            $url = substr($url, 0, -$l);
        }

        return $url;
    }

    protected function _getUrlSuffix()
    {
        $suffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
        if ($suffix && '/' != $suffix && '.' != $suffix[0]){
            $suffix = '.' . $suffix;
        }
        return $suffix;
    }

}