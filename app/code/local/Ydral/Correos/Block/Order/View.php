<?php

class Ydral_Correos_Block_Order_View extends Mage_Sales_Block_Order_View
{
    protected function _construct()
    {
        parent::_construct();
        if (in_array($this->getOrder()->getShippingMethod(), Mage::helper('correos')->getAllowedMethods()))
        {
            $this->setTemplate('correos/order_view.phtml');
        }
    }
}
