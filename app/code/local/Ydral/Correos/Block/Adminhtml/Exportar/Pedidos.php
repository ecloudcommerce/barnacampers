<?php

/**
 *
 */
class Ydral_Correos_Block_Adminhtml_Exportar_Pedidos extends Mage_Adminhtml_Block_Widget_Grid
{

    public function getDataExport($orderIds)
    {
        try
        {

            $_row = array();
            $_row[0] = array("REMITENTE", "Nombre", "Apellidos", "Nif", "Empresa", "Persona de Contacto", "Dirección", "Localidad", "Provincia", "CP", "Teléfono", "Email", "SMS",
                             "DESTINATARIO", "Nombre", "Apellidos", "Empresa", "Persona de Contacto", "Dirección", "Localidad", "Provincia", "CP", "ZIP", "Pais", "Teléfono", "Email", "Número SMS", "Idioma SMS",
                             "ENVÍO", "Producto", "Modalidad entrega", "Oficina Elegida", "Token", "IDTerminal", "Peso", "Largo", "Alto", "Ancho", "Importe seguro", "Importe reembolso", "Número cuenta", "Franja horaria", "Referencia");

            /**
             *
             */
            $rowFlag = 1;
            foreach ($orderIds as $orderId)
            {

                $_order = Mage::getModel('sales/order')->load($orderId);
                $_dataRecogida = Mage::getModel('correos/recoger')->getCheckoutData('order', $_order->getRealOrderId())->getFirstItem();
            	if($_order)
            	{

            	    //  REMITENTE
            	    $_row[$rowFlag][] = "";
            	    $remitente = Mage::getModel('correos/shipment')->getRemitenteArray();
            	    foreach ($remitente as $val)
            	    {
            	        $_row[$rowFlag][] = $val;
            	    }
            	    $remitente['CP'] = ' ' . $remitente['CP'] . ' ';

            	    //  DESTINATARIO
            	    $_row[$rowFlag][] = "";
                    if ($_order->getShippingMethod() == 'homepaq48_homepaq48' || $_order->getShippingMethod() == 'homepaq72_homepaq72')
                    {
                        $destinatario = Mage::getModel('correos/shipment')->getDataDestinatario($_order, $_dataRecogida->getMovilAsociado(), $_dataRecogida->getLanguage());
                        $destinatario = Mage::getModel('correos/shipment')->getDataDestinatarioHomepaq($_order, $destinatario, $_dataRecogida);
                        unset($destinatario['DireccionAlt']);
                    } else {
                        $destinatario = Mage::getModel('correos/shipment')->getDataDestinatario($_order, $_dataRecogida->getMovilAsociado(), $_dataRecogida->getLanguage());
                        if (!isset($destinatario['CP'])) array_splice($destinatario, 8, 0, "");
                        unset($destinatario['DireccionAlt']);
                    }
                    if (isset($destinatario['CP'])) $destinatario['CP'] = ' ' . $destinatario['CP'] . ' ';
                    $destinatario['ZIP'] = ' ' . $destinatario['ZIP'] . ' ';
                    $destinatario['Empresa'] = str_replace('<Empresa><![CDATA[', '', str_replace(']]></Empresa>', '', $destinatario['Empresa']));
                    $destinatario['PersonaContacto'] = substr(str_replace('<PersonaContacto><![CDATA[', '', str_replace(']]></PersonaContacto>', '', $destinatario['PersonaContacto'])), 0, 50);
                    foreach ($destinatario as $val)
            	    {
            	        $_row[$rowFlag][] = $val;
            	    }
            	    //  ENVIO
            	    $_row[$rowFlag][] = "";

                    //  opciones segun envio
                    if ($_order->getShippingMethod() == 'recogeroficina48_recogeroficina48') {
                        $_row[$rowFlag][]        = 'S0236';
                        $_row[$rowFlag][]        = 'LS';
                        $_row[$rowFlag][]        = $_dataRecogida->getCorreosRecogida();
                        $_row[$rowFlag][]        = '';
                        $_row[$rowFlag][]        = '';
                    } elseif ($_order->getShippingMethod() == 'recogeroficina72_recogeroficina72') {
                        $_row[$rowFlag][]        = 'S0133';
                        $_row[$rowFlag][]        = 'LS';
                        $_row[$rowFlag][]        = $_dataRecogida->getCorreosRecogida();
                        $_row[$rowFlag][]        = '';
                        $_row[$rowFlag][]        = '';
                    } elseif ($_order->getShippingMethod() == 'envio48_envio48') {
                        $_row[$rowFlag][]        = 'S0235';
                        $_row[$rowFlag][]        = 'ST';
                        $_row[$rowFlag][]        = '';
                        $_row[$rowFlag][]        = '';
                        $_row[$rowFlag][]        = '';
                    } elseif ($_order->getShippingMethod() == 'envio72_envio72') {
                        $_row[$rowFlag][]        = 'S0132';
                        $_row[$rowFlag][]        = 'ST';
                        $_row[$rowFlag][]        = '';
                        $_row[$rowFlag][]        = '';
                        $_row[$rowFlag][]        = '';
                    } elseif ($_order->getShippingMethod() == 'correosinter_correosinter') {
                        $_row[$rowFlag][]        = 'S0410';
                        $_row[$rowFlag][]        = 'ST';
                        $_row[$rowFlag][]        = '';
                        $_row[$rowFlag][]        = '';
                        $_row[$rowFlag][]        = '';
                    } elseif ($_order->getShippingMethod() == 'homepaq48_homepaq48') {
                        if (strtolower(substr(trim($_dataRecogida->getHomepaqId()), -1)) == 'd') {
                            //$_row[$rowFlag][]        = 'S0175';   //  4.0.2
                            $_row[$rowFlag][]        = 'S0178';
                        } else {    //  p
                            $_row[$rowFlag][]        = 'S0176';
                        }
                        $_row[$rowFlag][]       = 'ST';
                        $_row[$rowFlag][]       = '';
                        $_row[$rowFlag][]       = $_dataRecogida->getToken();
                        $_row[$rowFlag][]       = $_dataRecogida->getHomepaqId();
                    } elseif ($_order->getShippingMethod() == 'homepaq72_homepaq72') {
                        if (strtolower(substr(trim($_dataRecogida->getHomepaqId()), -1)) == 'd') {
                            //$_row[$rowFlag][]        = 'S0177';   //  4.0.2
                            $_row[$rowFlag][]        = 'S0178';
                        } else {    //  p
                            $_row[$rowFlag][]        = 'S0178';
                        }
                        $_row[$rowFlag][]       = 'ST';
                        $_row[$rowFlag][]       = '';
                        $_row[$rowFlag][]       = $_dataRecogida->getToken();
                        $_row[$rowFlag][]       = $_dataRecogida->getHomepaqId();
                    }
                    $_row[$rowFlag][]       = Mage::helper('correos')->getPesoPaquete($_order, 'gr', 'qty_ordered');

                    $order_collection   = Mage::getModel('correos/registro')->loadByOrder($orderId);
                    if ($order_collection)
                    {
                        $dataOrderCollection =  $order_collection->getFirstItem();

                        $_row[$rowFlag][]       = $dataOrderCollection->getMedidaFondo();   //  Largo
                        $_row[$rowFlag][]       = $dataOrderCollection->getMedidaAlto();   //  Alto
                        $_row[$rowFlag][]       = $dataOrderCollection->getMedidaAncho();   //  Ancho
                    } else {
                        $_row[$rowFlag][]       = '';   //  Largo
                        $_row[$rowFlag][]       = '';   //  Alto
                        $_row[$rowFlag][]       = '';   //  Ancho
                    }

                    if (Mage::getStoreConfig('correos/paquete/seguro'))
                    {
                        $_row[$rowFlag][]    = Mage::getStoreConfig('correos/paquete/importeseguro')*100;
                    } else {
                        $_row[$rowFlag][]    = '';
                    }
                    if ( ($_order->getPayment()->getMethodInstance()->getCode() == 'cashondelivery')
                         || ($_order->getPayment()->getMethodInstance()->getCode() == 'ig_cashondelivery')
                         || ($_order->getPayment()->getMethodInstance()->getCode() == 'phoenix_cashondelivery')
                         || ($_order->getPayment()->getMethodInstance()->getCode() == 'msp_cashondelivery')
                        )
                    {

                        $_total = $_order->getBaseGrandTotal();
                        $_total = number_format($_total, 2, '.', '');
                        if (!Mage::getModel('correos/shipment')->isInternationalOrder($_order) && $_total > 1000)
                        {
                            $_row[$rowFlag][]   = '';
                        } else {
                            $_row[$rowFlag][]   = $_total * 100;
                            //$_row[$rowFlag][]   = '';
                        }

                    } else {
                        $_row[$rowFlag][]   = '';
                    }

                    $_row[$rowFlag][]       = ' ' . (string)substr(Mage::getStoreConfig('correos/paquete/cuentareembolso'), 0, 20) . ' ';
                    $_row[$rowFlag][]       = ' ' . $_dataRecogida['horario'] . ' ';
                    $_row[$rowFlag][]       = $_order->getIncrementId();



            	}

            	$rowFlag++;

            }



            return $_row;

        } catch (Exception $e) {
            Mage::throwException(
                Mage::helper('correos')->__('No se han encontrado pedidos para exportar.')
            );
            return false;
        }


    }


}
