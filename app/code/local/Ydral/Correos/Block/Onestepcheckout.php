<?php 

class Ydral_Correos_Block_Onestepcheckout extends Idev_OneStepCheckout_Block_Checkout
{
    
    /*
    protected function _saveOrder()
    {
        Mage::helper('correos')->setOficinaCorreosRecogidaOrder();
        parent::_saveOrder();
    }
    */
    
    public function _handlePostData()
    {

        $checkoutHelper = Mage::helper('onestepcheckout/checkout');

        $post = $this->getRequest()->getPost();
        if(!$post) return;
        
        $billing_data = $this->getRequest()->getPost('billing', array());
        $shipping_data = $this->getRequest()->getPost('shipping', array());
        $_cp = Mage::app()->getRequest()->getPost('oficinas_correos_content_select');
        $shippingMethod = Mage::app()->getRequest()->getPost('shipping_method');
        $shipping_data = $checkoutHelper->load_exclude_data($shipping_data);
        
        
        if ( ($shippingMethod == 'recogeroficina48_recogeroficina48' || $shippingMethod == 'recogeroficina72_recogeroficina72') && (empty($_cp)) )
        {
            $this->formErrors['unknown_source_error'] = Mage::helper('correos')->__('No se ha especificado una oficina de recogida.');
            return;
        }


        $address = $this->getOnepage()->getQuote()->getShippingAddress();
        if(!isset($billing_data['use_for_shipping']) || $billing_data['use_for_shipping'] != '1') 
        {
            $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);
        } else {
            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);
        }
        if (!empty($customerAddressId)) {
            $customerAddress = Mage::getModel('customer/address')->load($customerAddressId);
            if ($customerAddress->getId()) {
                if ($customerAddress->getCustomerId() != $this->getOnepage()->getQuote()->getCustomerId()) {
                    return array('error' => 1,
                        'message' => Mage::helper('checkout')->__('Customer Address is not valid.')
                    );
                }
                $address->importCustomerAddress($customerAddress);
            }
        }
        
        if (!isset($post['phone_correos']) || $post['phone_correos'] == '')
        {
            $_phone = $address->getTelephone();
        } else {
            $_phone = $post['phone_correos'];
        }
        if ( ($shippingMethod == 'recogeroficina48_recogeroficina48' || $shippingMethod == 'recogeroficina72_recogeroficina72') && (empty($_phone)) )
        {
            $this->formErrors['unknown_source_error'] = Mage::helper('correos')->__('Debe indicar un tel&eacute;fono m&oacute;vil para poder realizar la entrega del env&iacute;o.');
            return;
        } elseif ( ($shippingMethod == 'recogeroficina48_recogeroficina48' || $shippingMethod == 'recogeroficina72_recogeroficina72') && (!Mage::helper('correos')->validarMovil($_phone)) ) {
            $this->formErrors['unknown_source_error'] = Mage::helper('correos')->__('N&uacute;mero de tel&eacute;fono m&oacute;vil no v&aacute;lido.');
            return;
        }
        
        
        /**
         *
         */
        $_homepaq = Mage::app()->getRequest()->getPost('selectedpaq_code');
        $_phoneHomepaq = Mage::app()->getRequest()->getPost('phone_correos');
        if ( ($shippingMethod == 'homepaq48_homepaq48' || $shippingMethod == 'homepaq72_homepaq72') && (empty($_homepaq)) )
        {
            return array('error' => -1, 'message' => $this->_helper->__('Selecciona un Terminal CorreosPaq antes de continuar.'));
        } else if ( ($shippingMethod == 'homepaq48_homepaq48' || $shippingMethod == 'homepaq72_homepaq72') && (!Mage::helper('correos')->validarMovil($_phoneHomepaq)) ) {
            return array('error' => -1, 'message' => $this->_helper->__('N�mero de tel�fono m�vil no v�lido.'));
        }
        
        
        parent::_handlePostData();
        
        
    }
    
}