<?php 

class Ydral_Correos_Helper_Onestepcheckout extends Idev_OneStepCheckout_Helper_Checkout
{

    public function saveShippingMethod($shippingMethod)
    {
        
        $_cp = Mage::app()->getRequest()->getPost('oficinas_correos_content_select');
        if (empty($_cp)) $_cp = Mage::getSingleton('checkout/type_onepage')->getQuote()->getShippingAddress()->getPostcode();
        if ( ($shippingMethod == 'recogeroficina48_recogeroficina48' || $shippingMethod == 'recogeroficina72_recogeroficina72') && (empty($_cp)) )
        {
            return array('error' => -1, 'message' => Mage::helper('correos')->__('No se ha especificado una oficina de recogida.'));
        }
        
        return parent::saveShippingMethod($shippingMethod);
            
    }

}