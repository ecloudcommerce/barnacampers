<?php 

/**
 *
 */
class Ydral_Correos_Helper_Shipping extends Mage_Shipping_Helper_Data
{
    
    public function getTrackingPopupUrlBySalesModel($model)
    {        
        
        if ($model instanceof Mage_Sales_Model_Order) {
            $order = $model;
        } elseif ($model instanceof Mage_Sales_Model_Order_Shipment) {
            $order_id = $model->getOrderId();
            $order = Mage::getModel('sales/order')->load($order_id);
        } elseif ($model instanceof Mage_Sales_Model_Order_Shipment_Track) {
            $order_id = $model->getOrderId();
            $order = Mage::getModel('sales/order')->load($order_id);
        }        
        
        if (!$order) return parent::getTrackingPopupUrlBySalesModel($model);
        

        if (in_array($order->getShippingMethod(), Mage::helper('correos')->getAllowedMethods()))
        {
            $param = array(
                'hash' => Mage::helper('core')->urlEncode("order_id:{$order->getId()}:{$order->getProtectCode()}")
            );
        
            $storeId = is_object($order) ? $order->getStoreId() : null;
            $storeModel = Mage::app()->getStore($storeId);
            return $storeModel->getUrl('correos/tracking/popup', $param);
        } else { 
            return parent::getTrackingPopupUrlBySalesModel($model);
        }
    }
    
}
