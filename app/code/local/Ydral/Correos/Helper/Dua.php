<?php

/**
 *
 */
class Ydral_Correos_Helper_Dua extends Mage_Core_Helper_Abstract
{

    protected $_allowedMethods = array (
            '01'     => 'alava',
            '02'     => 'albacete',
            '03'     => 'alicante',
            '04'     => 'almeria',
            '05'     => 'avila',
            '06'     => 'badajoz',
            '07'     => 'baleares',
            '08'     => 'barcelona',
            '09'     => 'burgos',
            '10'     => 'caceres',
            '11'     => 'cadiz',
            '12'     => 'castellon',
            '13'     => 'ciudad real',
            '14'     => 'cordoba',
            '15'     => 'a coruña',
            '16'     => 'cuenca',
            '17'     => 'girona',
            '18'     => 'granada',
            '19'     => 'guadalajara',
            '20'     => 'guipuzcoa',
            '21'     => 'huelva',
            '22'     => 'huesca',
            '23'     => 'jaen',
            '24'     => 'leon',
            '25'     => 'lleida',
            '26'     => 'la rioja',
            '27'     => 'lugo',
            '28'     => 'madrid',
            '29'     => 'malaga',
            '30'     => 'murcia',
            '31'     => 'navarra',
            '32'     => 'ourense',
            '33'     => 'asturias',
            '34'     => 'palencia',
            '35'     => 'las palmas',
            '36'     => 'pontevedra',
            '37'     => 'salamanca',
            '38'     => 'santa cruz de tenerife',
            '39'     => 'cantabria',
            '40'     => 'segovia',
            '41'     => 'sevilla',
            '42'     => 'soria',
            '43'     => 'tarragona',
            '44'     => 'teruel',
            '45'     => 'toledo',
            '46'     => 'valencia',
            '47'     => 'valladolid',
            '48'     => 'vizcaya',
            '49'     => 'zamora',
            '50'     => 'zaragoza',
            '51'     => 'ceuta',
            '52'     => 'melilla',
	);


	protected $_typeGoods = array(
		'300' => 'Adornos de cerámica, estatuillas',
		'301' => 'Adornos para fiestas de Navidad',
		'302' => 'Agujas de coser, punto, imperdibles',
		'303' => 'Aparatos fotografia - Filtro para cámaras fotogr',
		'304' => 'Aparatos fotografia - Flashes',
		'305' => 'Aparatos fotografia - Lente para cámara',
		'306' => 'Aparatos de optica - Binoculares',
		'307' => 'Aparatos de optica - Gafas (de ver)',
		'308' => 'Aparatos de optica- Gafas de sol',
		'309' => 'Aparatos de optica - Monturas gafas',
		'310' => 'Aparatos de optica- cristales, lentes gafas',
		'311' => 'Aparatos de optica- Lentes de contacto',
		'312' => 'Aparatos laboratorio - microscopio',
		'313' => 'Aparatos medicoquirúrjicos- Audífono',
		'314' => 'Aparatos medicoquirurjicos- Marcapasos',
		'315' => 'Aparatos relojería - adornos, piedras',
		'316' => 'Aparatos relojería - Despertador',
		'317' => 'Aparatos relojeria - Pulsera para reloj',
		'318' => 'Aparatos relojeria - Reloj de pulsera, bolsillo',
		'319' => 'Aparatos relojería Reloj Muelles,tornillos',
		'320' => 'Aperitivos salados',
		'321' => 'Armas decorativas Sables, espadas, bayoneta',
		'322' => 'Articulos de Bisutería',
		'323' => 'Artículos de ceramica, porcelana',
		'324' => 'Artículos de cuero - bolso, mochila',
		'325' => 'Artículos de cuero - Cinturones, guantes',
		'326' => 'Artículos de metal - Cucharas, tenedores, cucharo',
		'327' => 'Artículos de metal - cuchillas, abrecartas, raspa',
		'328' => 'Artículos de metal - Cuchillos, navajas',
		'329' => 'Artículos de orfebrería',
		'330' => 'Artículos de pesca - anzuelos, carretes',
		'331' => 'Artículos de piel Billetera, monedero, pitillera',
		'332' => 'Azúcar de caña o remolacha',
		'333' => 'Bebida - Vermú',
		'334' => 'Bebida alcohólicas - Ginebra',
		'335' => 'Bebidas - Agua',
		'336' => 'Bebidas - Cerveza',
		'337' => 'Bebidas - Sidra',
		'338' => 'Vinagre',
		'339' => 'Bebidas - Vino',
		'340' => 'Bebidas - Vino espumoso',
		'341' => 'Bebidas - Zumos de frutas alcohóicas',
		'342' => 'Bebidas alcohólicas - Vodka',
		'343' => 'Bebidas alcohólicas - licores',
		'344' => 'Bebidas alcohólicas - Whisky',
		'345' => 'Bebidas - Gin y Ginebra',
		'346' => 'Bebidas - Mosto',
		'347' => 'Bebidas - Ron y aguardiente de caña',
		'348' => 'Bebidas sin alcohol',
		'349' => 'Betunes, creamas para zapatos o cuero',
		'350' => 'Bolígrafos',
		'351' => 'Bolsos de mano de piel',
		'352' => 'Botones',
		'353' => 'Brochas de afeitar',
		'354' => 'Bulbos, cebollas de flores',
		'355' => 'Cacao en polvo',
		'356' => 'Café',
		'357' => 'Café descafeinado',
		'358' => 'Cajas, bolsas de papel',
		'359' => 'Calculadoras',
		'360' => 'Calendarios',
		'361' => 'Calzado de deporte',
		'362' => 'Candados, cerrojos, llaves',
		'363' => 'Caña de pescar',
		'364' => 'Cepillo para el pelo',
		'365' => 'Cepillos de dientes',
		'366' => 'Cereales - Arroz',
		'367' => 'Cereales - Copos de maiz y similiares',
		'368' => 'Cereales Avena',
		'369' => 'Cereales Maiz',
		'370' => 'Cereales Trigo',
		'371' => 'Chicles',
		'372' => 'Chocolate, cacao',
		'373' => 'Cierres, hebillas, ganchos, ojales de metal común',
		'374' => 'Comida - Conservas - Aceitunas',
		'375' => 'Comida - Conservas de pescado',
		'376' => 'Comida- Caviar y sus sucedáneos',
		'377' => 'Comida- Chorizo',
		'378' => 'Comida- Conservas de carne',
		'379' => 'Comida- Conservas de marisco y moluscos',
		'380' => 'Comida- Hígado de oca, pato, Foie grass',
		'381' => 'Comida- Jamón',
		'382' => 'Comida- Lomo',
		'383' => 'Comida- paletilla jamón',
		'384' => 'Comida- pata de Jamón',
		'385' => 'Comida- Salchichas',
		'386' => 'Comida- Salchichón',
		'387' => 'Compresas, tampones higiénicos',
		'388' => 'Conservas - esparrágos',
		'389' => 'Conservas de legumbres',
		'390' => 'Conservas de Setas, hongos, trufas',
		'391' => 'Conservas de tomate',
		'392' => 'Conservas de verduras',
		'393' => 'Cosméticos - champú, Gel',
		'394' => 'Cosméticos - dentífricos',
		'395' => 'Cosméticos - espuma afeitar',
		'396' => 'Cosméticos - lacas para el cabello',
		'397' => 'Cosméticos - maquillajes, bronceadores',
		'398' => 'Cosméticos - pintalabios',
		'399' => 'Cosméticos - sombras de ojos, polvos compactos',
		'400' => 'Cosméticos- Desodorantes corporales',
		'401' => 'Cremalleras',
		'402' => 'Cuadernos',
		'403' => 'Cuadernos para dibujar o colorear, para niños',
		'404' => 'Deportes - Balones de fútbol, balonmano, balonces',
		'405' => 'Deportes - Pelotas de Golf',
		'406' => 'Deportes - Pelotas de tenis',
		'407' => 'Deportes -Patines',
		'408' => 'Diccionarios y enciclopedias, fascículos',
		'409' => 'Dispositivos electricos - Auriculares',
		'410' => 'Dispositivos electricos - Depiladora electrica',
		'411' => 'Dispositivos electricos - Linterna',
		'412' => 'Dispositivos electricos - Maquina cortar pelo',
		'413' => 'Dispositivos electricos - Maquinilla afeitar',
		'414' => 'Dispositivos electricos - Radiocasete de bolsillo',
		'415' => 'Dispositivos electricos - Reproductor de CD',
		'416' => 'Dispositivos electricos - Reproductor de DVD',
		'417' => 'Dispositivos electricos - Tarjetas circuito electr',
		'418' => 'Dispositivos electricos - Teléfono móvil',
		'419' => 'Dispositivos electricos - Videocámara portatil',
		'420' => 'Dispositivos electricos - Walkman',
		'421' => 'Dispositivos electricos -Reproductor de MP3',
		'422' => 'Dispositivos electricos- Secador de pelo',
		'423' => 'Edredones, cojines, almohadas',
		'424' => 'Encendedores de bolsillo, de gas, no recargables',
		'425' => 'Especias Azafrán',
		'426' => 'Especias Cúrcuma',
		'427' => 'Especias Curri',
		'428' => 'Especias Jengibre',
		'429' => 'Especias Vainilla',
		'430' => 'Estampas, grabados y fotografías',
		'431' => 'Fechadores, selladores',
		'432' => 'Flores artificiales, plumas, pelucas, barbas, mech',
		'433' => 'Flores, capullos, adornos decorativos',
		'434' => 'Fruta deshidratada',
		'435' => 'Frutos secos almendras',
		'436' => 'Frutos secos anacardo',
		'437' => 'Frutos secos avellanas',
		'438' => 'Frutos secos Dátiles',
		'439' => 'Frutos secos Higos',
		'440' => 'Frutos secos Nueces',
		'441' => 'Frutos secos Pistachos',
		'442' => 'Frutos secos Uvas secas - pasas',
		'443' => 'Galletas, pastas, pasteles',
		'444' => 'Gasas, vendas',
		'445' => 'Gofres y obleas',
		'446' => 'Goma de Borrar',
		'447' => 'Grapas, clips',
		'448' => 'Guirnaldas electricas de Navidad',
		'449' => 'Harina',
		'450' => 'Herramientas - Destornilladores',
		'451' => 'Herramientas - Cepillos y herramientas para madera',
		'452' => 'Herramientas - limas, alicates, pinzas, cizallas',
		'453' => 'Herramientas- Martillos',
		'454' => 'Herramientas Llaves de ajuste manuales, llave ingl',
		'455' => 'Huevos',
		'456' => 'Impresos publicitarios, catálogos comerciales y s',
		'457' => 'Instrumento musical - Guitarra',
		'458' => 'Instrumento musical - Armónica',
		'459' => 'Instrumento musical - Cajas de Música',
		'460' => 'Instrumento musical - Clarinetes',
		'461' => 'Instrumento musical - piezas de instrumentos',
		'462' => 'Instrumento musical -Violin',
		'463' => 'Jabón en pastillas',
		'464' => 'Joyas - Artículos para joyería',
		'465' => 'Joyas - Diamantes',
		'466' => 'Joyas - Oro',
		'467' => 'Joyas - Plata',
		'468' => 'Joyas -Perlas',
		'469' => 'Juegos - pin pon',
		'470' => 'Juegos - Videojuegos',
		'471' => 'Juguetes Cartas de mesa, Naipes',
		'472' => 'Juguetes Muñecas',
		'473' => 'Juguetes Rompecabezas',
		'474' => 'Juguetes ropa y accesorios muñecos',
		'475' => 'Juguetes Videojuego',
		'476' => 'JuguetesTrenes, coches eléctricos',
		'477' => 'Lámpara eléctrica de mesa, oficina',
		'478' => 'Lámparas o aparatos eléctricos de alumbrado',
		'479' => 'Lápices portaminas',
		'480' => 'Leche',
		'481' => 'Leche concentrada, condensada',
		'482' => 'Leche en polvo',
		'483' => 'levadura en polvo para esponjar masas',
		'484' => 'Libros registro, libros de contabilidad, talonario',
		'485' => 'Libros, folletos, e impresos similares',
		'486' => 'Manteles y servilletas de papel',
		'487' => 'Mantequilla',
		'488' => 'Marcos de madera',
		'489' => 'Material cestería - mimbre, rafia',
		'490' => 'Medicamento - Antisueros',
		'491' => 'Medicamento - Cortisona',
		'492' => 'Medicamento - Insulina',
		'493' => 'Medicamento - Penicilinas',
		'494' => 'Medicamento - Vitaminas',
		'495' => 'Medicamento -Vacunas para animales',
		'496' => 'Medicamento -Vacunas para uso humano',
		'497' => 'Medicamentos',
		'498' => 'Medicamentos - Antibióticos',
		'499' => 'Mermeladas, Confituras, jaleas',
		'500' => 'Miel Natural',
		'501' => 'Monedas.',
		'502' => 'Muesli y similares',
		'503' => 'Objetos arte, colección - pinturas, dibujos a man',
		'504' => 'Objetos arte, colección - sellos, estampillas',
		'505' => 'Objetos arte, collección - Escultura de arte',
		'506' => 'Objetos arte, collección - Grabados, estampas, li',
		'507' => 'Objetos arte, collección- Piezas de colección',
		'508' => 'Pan y similares',
		'509' => 'Pañales de bebés',
		'510' => 'Pañuelos, toallitas de desmaquillar y toallas',
		'511' => 'Papel higiénico',
		'512' => 'Paraguas, sombrillas, quitasoles, fustas y latigos',
		'513' => 'Partes o accesorios de bicicletas o motos',
		'514' => 'Partes vehículos - Amortiguador vehiculos',
		'515' => 'Partes vehículos - Freno de coche',
		'516' => 'Partes vehículos - Volante coche',
		'517' => 'Partes vehículos -Cinturones de seguridad de coch',
		'518' => 'Pasta alimenticia, esapagueti, macarrones',
		'519' => 'Pegamentos, colas o adhesivos',
		'520' => 'Peines, peinetas',
		'521' => 'Películas fotográficas',
		'522' => 'Perchas de madera',
		'523' => 'Perfume, colonia',
		'524' => 'Piedras preciosas o semi - Cuarzo',
		'525' => 'Piedras preciosas o semi - Rubíes, zafiros y esme',
		'526' => 'Piedras preciosas o semipreciosas',
		'527' => 'Pinceles y brochas',
		'528' => 'Pintura artística',
		'529' => 'Pipas y cachimbas',
		'530' => 'Plantas, bulbos',
		'531' => 'Plástico - artículos de adorno',
		'532' => 'Plástico - Vajilla, servicio de mesa o de cocina',
		'533' => 'Plástico -Artículos de oficina y artículos esco',
		'534' => 'Portatiles hasta 10 kg',
		'535' => 'Pralinés',
		'536' => 'Preparados alimenticios para animales',
		'537' => 'Preparados alimenticios para bebés - cereales',
		'538' => 'Preservativos',
		'539' => 'Productos farmaceuticos- Apósitos',
		'540' => 'Productos farmaceuticos- Botiquines primeros auxil',
		'541' => 'Queso curado',
		'542' => 'Queso fresco, de untar',
		'543' => 'Raíces o tuberculos de Trufas frescas o refrigera',
		'544' => 'Ropa - Calzoncillos',
		'545' => 'Ropa - Camisas de punto hombre',
		'546' => 'Ropa - Camisas de punto mujer',
		'547' => 'Ropa Abrigos, chaquetones, capas, cazadoras',
		'548' => 'Ropa Albornoces de baño',
		'549' => 'Ropa Batas de casa',
		'550' => 'Ropa blanca de baño y de cocina',
		'551' => 'Ropa Bragas, saltos de cama',
		'552' => 'Ropa Camisetas',
		'553' => 'Ropa Camisones, pijamas',
		'554' => 'Ropa chales, pañuelos de cuello, bufandas, mantil',
		'555' => 'Ropa Combinaciones, enaguas',
		'556' => 'Ropa Corbatas o pajaritas',
		'557' => 'Ropa de cama, tejida o de ganchillo',
		'558' => 'Ropa deportiva (chandal, mono, ropa de esquí, ba',
		'559' => 'Ropa Guantes, mitones y manoplas',
		'560' => 'Ropa impermeables',
		'561' => 'Ropa Jerseys',
		'562' => 'Ropa leotardos, medias, calcetines',
		'563' => 'Ropa Mantelería, tejida o de ganchillo',
		'564' => 'Ropa para bebés',
		'565' => 'Ropa Sujetadores, fajas, corsés, tirantes, liguer',
		'566' => 'Ropa Trajes , conjuntos, chaquetas hombres',
		'567' => 'Ropa Trajes sastre, vestidos, faldas mujer',
		'568' => 'Sales perfumadas y demás preparaciones para el ba',
		'569' => 'Aceite de oliva',
		'570' => 'Semillas de hortalizas',
		'571' => 'Sobres de papel',
		'572' => 'Sombreros, tocados, redecillas para el pelo',
		'573' => 'Sopas y salsas preparadas',
		'574' => 'Suplementos alimenticios (multivitaminas/minerales',
		'575' => 'Tabaco cigarrillos',
		'576' => 'Tabaco puros',
		'577' => 'Tapones de corcho',
		'578' => 'Té negro',
		'579' => 'Té Verde',
		'580' => 'Textiles - algodón',
		'581' => 'Textiles - lana',
		'582' => 'Textiles - nailon',
		'583' => 'Textiles - poliésteres',
		'584' => 'Textiles - seda',
		'585' => 'Tornillos, pernos, tuercas',
		'586' => 'Velas, cirios',
		'587' => 'Verduras deshidratadas',
		'588' => 'Vidrio vasos, copas jarras',
		'999' => 'Textil y calzado'
    );

    /**
     *
     */
    public function getXmlDua(array $data)
    {

        if (empty($data)) return false;

        $xmlSend = <<<XML
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:prer="http://www.correos.es/iris6/services/preregistroetiquetas">
<soapenv:Header/>
<soapenv:Body>
<prer:SolicitudDocumentacionAduanera>
<prer:TipoESAD>{$data['type_doc']}</prer:TipoESAD>
{$data['dataAuth']}
<prer:Provincia>{$data['Provincia']}</prer:Provincia>
<prer:PaisDestino>{$data['PaisDestino']}</prer:PaisDestino>
<prer:NombreDestinatario>{$data['NombreDestinatario']}</prer:NombreDestinatario>
<prer:NumeroEnvios>{$data['NumeroEnvios']}</prer:NumeroEnvios>
<prer:LocalidadFirma>{$data['LocalidadFirma']}</prer:LocalidadFirma>
</prer:SolicitudDocumentacionAduanera>
</soapenv:Body>
</soapenv:Envelope>
XML;

        return $xmlSend;

    }


    /**
     *
     */
    public function getXmlCn23cp71($etiqueta)
    {

        if (empty($etiqueta)) return false;

        $xmlSend = <<<XML
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:prer="http://www.correos.es/iris6/services/preregistroetiquetas">
<soapenv:Header/>
<soapenv:Body>
<prer:SolicitudDocumentacionAduaneraCN23CP71>
<prer:codCertificado>{$etiqueta}</prer:codCertificado>
</prer:SolicitudDocumentacionAduaneraCN23CP71>
</soapenv:Body>
</soapenv:Envelope>
XML;

        return $xmlSend;

    }


    /**
     *
     */
    public function getCodeRegion($region)
    {
        if ($key = array_search(strtolower($region), $this->_allowedMethods))
        {
            return $key;
        }
        return false;
    }

    /**
     *
     */
    public function getTypeGoods()
    {
        return $this->_typeGoods;
    }

    /**
     *
     */
    public function getProvinciasDua($storeId = '')
    {
        $_provincias = Mage::helper('correos')->getValueConfig('provincias', 'dua', $storeId);
        $_provincias = explode(',', $_provincias);
        return $_provincias;
    }

    /**
     *
     */
    public function isDua($order)
    {
        $shippingMethod = $order->getShippingMethod();
        $storeId        = $order->getStoreId();
        $_origen    = Mage::getModel('directory/region')->load(Mage::helper('correos')->getValueConfig('region_id', 'remitente', $storeId))->getName();
        $_destino   = $order->getShippingAddress()->getRegion();

        if ($shippingMethod == 'homepaq48_homepaq48' || $shippingMethod == 'homepaq72_homepaq72')
        {
            $_dataRecogida = Mage::getModel('correos/recoger')->getCheckoutData('order', $order->getRealOrderId())->getFirstItem();
            $_infoPunto = Mage::getModel('correos/homepaq')->dataPunto($_dataRecogida['info_punto']);
            if (is_array($_infoPunto)) {
                $_destino = trim(strtolower($_infoPunto['region']));
            }
        }


        $isDua = false;
        $provinciasDua = Mage::helper('correos/dua')->getProvinciasDua($storeId);
        if (in_array($_origen, $provinciasDua))
        {
            $isDua = true;
        } elseif ($order->getShippingAddress()->getCountryId() != 'ES' && !Mage::getModel('correos/shipment')->isEuropean($order->getShippingAddress()->getCountryId())) {
            $isDua = true;
        } elseif (in_array($_destino, $provinciasDua)) {
            $isDua = true;
        }

        return $isDua;

    }


    /**
     *
     */
    public function isCn23cp71($order)
    {
        $shippingMethod = $order->getShippingMethod();
        $storeId        = $order->getStoreId();
        $_origen    = Mage::helper('correos')->getValueConfig('country_id', 'remitente', $storeId);
        $_destino   = $order->getShippingAddress()->getCountryId();

        $isCn23cp71 = false;
        if ($shippingMethod == 'envio48_envio48' || $shippingMethod == 'envio72_envio72')
        {
            if ($_origen == 'AD' || $_destino == 'AD') {
                $isCn23cp71 = true;
            }
        } elseif ($shippingMethod == 'correosinter_correosinter') {
            $isCn23cp71 = true;
        }

        return $isCn23cp71;

    }


}
