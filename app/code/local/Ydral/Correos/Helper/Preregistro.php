<?php 

/**
 *
 */
class Ydral_Correos_Helper_Preregistro extends Mage_Core_Helper_Abstract
{

    /**
     * @param array $data
     * @return string
     */
    public function getXmlPreregistro(array $data)
    {
     
        if (empty($data)) return false;
        
        
        $xmlSend = '';
        $xmlSend.= '<?xml version="1.0" encoding="utf-8"?>';
        $xmlSend.= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="http://www.correos.es/iris6/services/preregistroetiquetas">';
        $xmlSend.= '<soapenv:Header/>';
        $xmlSend.= '<soapenv:Body>';
        $xmlSend.= '<PreregistroEnvio>';
        $xmlSend.= '<fechaOperacion>'.$data['fechaOperacion'].'</fechaOperacion>';
        $xmlSend.= $data['dataAuth'];
        $xmlSend.= '<Care>000000</Care>';
        $xmlSend.= '<TotalBultos>1</TotalBultos>';
        $xmlSend.= '<ModDevEtiqueta>2</ModDevEtiqueta>';
        
        $xmlSend.= '<Remitente>';
        $xmlSend.= '<Identificacion>';
        $xmlSend.= (empty($data['datosRemitente']['Nombre'])?'':'<Nombre><![CDATA['.$data['datosRemitente']['Nombre'].']]></Nombre>');
        $xmlSend.= (empty($data['datosRemitente']['Apellidos'])?'':'<Apellido1><![CDATA['.$data['datosRemitente']['Apellidos'].']]></Apellido1>');
        $xmlSend.= (empty($data['datosRemitente']['Nif'])?'':'<Nif><![CDATA['.$data['datosRemitente']['Nif'].']]></Nif>');
        $xmlSend.= (empty($data['datosRemitente']['Empresa'])?'':'<Empresa><![CDATA['.$data['datosRemitente']['Empresa'].']]></Empresa>');
        $xmlSend.= (empty($data['datosRemitente']['Personacontacto'])?'':'<PersonaContacto><![CDATA['.$data['datosRemitente']['Personacontacto'].']]></PersonaContacto>');
        $xmlSend.= '</Identificacion>';
        $xmlSend.= '<DatosDireccion>';
        $xmlSend.= (empty($data['datosRemitente']['Direccion'])?'':'<Direccion><![CDATA['.$data['datosRemitente']['Direccion'].']]></Direccion>');
        $xmlSend.= (empty($data['datosRemitente']['Localidad'])?'':'<Localidad><![CDATA['.$data['datosRemitente']['Localidad'].']]></Localidad>');
        $xmlSend.= (empty($data['datosRemitente']['Provincia'])?'':'<Provincia><![CDATA['.$data['datosRemitente']['Provincia'].']]></Provincia>');
        $xmlSend.= '</DatosDireccion>';
        $xmlSend.= $data['datosRemitente']['CP'];
        $xmlSend.= (empty($data['datosRemitente']['Telefono'])?'':'<Telefonocontacto><![CDATA['.$data['datosRemitente']['Telefono'].']]></Telefonocontacto>');
        $xmlSend.= (empty($data['datosRemitente']['Email'])?'':'<Email><![CDATA['.$data['datosRemitente']['Email'].']]></Email>');
        $xmlSend.= (empty($data['datosRemitente']['Sms'])?'':'<DatosSMS><NumeroSMS><![CDATA['.$data['datosRemitente']['Sms'].']]></NumeroSMS><Idioma>1</Idioma></DatosSMS>');
        $xmlSend.= '</Remitente>';
        
        
        
        
        $xmlSend = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="http://www.correos.es/iris6/services/preregistroetiquetas">
<soapenv:Header/>
<soapenv:Body>
<PreregistroEnvio>
<fechaOperacion>{$data['fechaOperacion']}</fechaOperacion>
{$data['dataAuth']}
<Care>000000</Care>
<TotalBultos>1</TotalBultos>
<ModDevEtiqueta>2</ModDevEtiqueta>
<Remitente>
<Identificacion>
<Nombre><![CDATA[{$data['datosRemitente']['Nombre']}]]></Nombre>
<Apellido1><![CDATA[{$data['datosRemitente']['Apellidos']}]]></Apellido1>
<Nif><![CDATA[{$data['datosRemitente']['Nif']}]]></Nif>
<Empresa><![CDATA[{$data['datosRemitente']['Empresa']}]]></Empresa>
<PersonaContacto><![CDATA[{$data['datosRemitente']['Personacontacto']}]]></PersonaContacto>
</Identificacion>
<DatosDireccion>
<Direccion><![CDATA[{$data['datosRemitente']['Direccion']}]]></Direccion>
<Localidad><![CDATA[{$data['datosRemitente']['Localidad']}]]></Localidad>
<Provincia><![CDATA[{$data['datosRemitente']['Provincia']}]]></Provincia>
</DatosDireccion>
{$data['datosRemitente']['CP']}
<Telefonocontacto><![CDATA[{$data['datosRemitente']['Telefono']}]]></Telefonocontacto>
<Email><![CDATA[{$data['datosRemitente']['Email']}]]></Email>
<DatosSMS>
<NumeroSMS><![CDATA[{$data['datosRemitente']['Sms']}]]></NumeroSMS>
<Idioma>1</Idioma>
</DatosSMS>
</Remitente> 
    <Destinatario> 
        <Identificacion> 
            <Nombre><![CDATA[{$data['DatosDestinatario']['Nombre']}]]></Nombre>
            <Apellido1><![CDATA[{$data['DatosDestinatario']['Apellidos']}]]></Apellido1>
            {$data['DatosDestinatario']['Empresa']}
            {$data['DatosDestinatario']['PersonaContacto']}
        </Identificacion>
        <DatosDireccion>
            <Direccion><![CDATA[{$data['DatosDestinatario']['Direccion']}]]></Direccion>
            <Localidad><![CDATA[{$data['DatosDestinatario']['Localidad']}]]></Localidad>
            <Provincia><![CDATA[{$data['DatosDestinatario']['Provincia']}]]></Provincia>
        </DatosDireccion>
        {$data['DatosDestinatario']['CP']}
        <ZIP><![CDATA[{$data['DatosDestinatario']['ZIP']}]]></ZIP>
        <Pais><![CDATA[{$data['DatosDestinatario']['Pais']}]]></Pais>
        <Telefonocontacto><![CDATA[{$data['DatosDestinatario']['Telefono']}]]></Telefonocontacto>
        <Email><![CDATA[{$data['DatosDestinatario']['Email']}]]></Email>
        <DatosSMS>
            <NumeroSMS><![CDATA[{$data['DatosDestinatario']['Sms']}]]></NumeroSMS>
            <Idioma><![CDATA[{$data['DatosDestinatario']['Idioma']}]]></Idioma>
        </DatosSMS>
    </Destinatario> 
    <Envio>
        <CodProducto>{$data['DatosEnvio']['CodProducto']}</CodProducto>
        <ReferenciaCliente>{$data['DatosEnvio']['ReferenciaCliente']}</ReferenciaCliente>
        <TipoFranqueo>{$data['DatosEnvio']['TipoFranqueo']}</TipoFranqueo>
        <ModalidadEntrega>{$data['DatosEnvio']['ModalidadEntrega']}</ModalidadEntrega>
        {$data['DatosEnvio']['OficinaElegida']}
        <Pesos>
            <Peso>
                <TipoPeso>{$data['DatosEnvio']['Peso_Tipo']}</TipoPeso>
                <Valor>{$data['DatosEnvio']['Peso_Valor']}</Valor>
            </Peso>
        </Pesos>
        {$data['DatosEnvio']['Largo']}
        {$data['DatosEnvio']['Alto']}
        {$data['DatosEnvio']['Ancho']}
        <ValoresAnadidos>
            {$data['DatosEnvio']['ValoresAnadidos']['ImporteSeguro']}
            {$data['DatosEnvio']['ValoresAnadidos']['Reembolso']}
            {$data['DatosEnvio']['ValoresAnadidos']['FranjaHorariaConcertada']}
         </ValoresAnadidos>
         <Observaciones1><![CDATA[{$data['DatosDestinatario']['DireccionAlt']}]]></Observaciones1>
         <Observaciones2><![CDATA[{$data['observaciones']}]]></Observaciones2>
        {$data['DatosEnvio']['InstruccionesDevolucion']}
        {$data['DatosEnvio']['Aduana']['tipoAduana']}
        {$data['DatosEnvio']['Homepaq']['tipoHomepaq']}
    </Envio>
</PreregistroEnvio>
</soapenv:Body>
</soapenv:Envelope>
XML;
        
        return $xmlSend;
        
    }

}