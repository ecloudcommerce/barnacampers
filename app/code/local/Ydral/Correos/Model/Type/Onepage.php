<?php

/**
 * One page checkout processing model
 */
class Ydral_Correos_Model_Type_Onepage extends Mage_Checkout_Model_Type_Onepage
{
    
    /**
     * Specify quote shipping method
     *
     * @param   string $shippingMethod
     * @return  array
     */
    public function saveShippingMethod($shippingMethod)
    {

        /**
         *
         */
        $_cp = Mage::app()->getRequest()->getPost('oficinas_correos_content_select');
        
        if ( ($shippingMethod == 'recogeroficina48_recogeroficina48' || $shippingMethod == 'recogeroficina72_recogeroficina72') && (empty($_cp)) )
        {
            return array('error' => -1, 'message' => $this->_helper->__('No se ha especificado una oficina de recogida.'));
        }

        /**
         *
         */
        $_phone = Mage::app()->getRequest()->getPost('phone_correos');
        if ( ($shippingMethod == 'recogeroficina48_recogeroficina48' || $shippingMethod == 'recogeroficina72_recogeroficina72') && (empty($_phone)) )
        {
            return array('error' => -1, 'message' => $this->_helper->__('Debe indicar un teléfono móvil para poder realizar la entrega del envío.'));
        } elseif ( ($shippingMethod == 'recogeroficina48_recogeroficina48' || $shippingMethod == 'recogeroficina72_recogeroficina72') && (!Mage::helper('correos')->validarMovil($_phone)) ) {
            return array('error' => -1, 'message' => $this->_helper->__('Número de teléfono móvil no válido.'));
        }

        /**
         *
         */
        $_homepaq = Mage::app()->getRequest()->getPost('selectedpaq_code');
        $_phoneHomepaq = Mage::app()->getRequest()->getPost('phone_correos');
        if ( ($shippingMethod == 'homepaq48_homepaq48' || $shippingMethod == 'homepaq72_homepaq72') && (empty($_homepaq)) )
        {
            return array('error' => -1, 'message' => $this->_helper->__('Selecciona un Terminal CorreosPaq antes de continuar.'));
        } else if ( ($shippingMethod == 'homepaq48_homepaq48' || $shippingMethod == 'homepaq72_homepaq72') && (!Mage::helper('correos')->validarMovil($_phoneHomepaq)) ) {
            return array('error' => -1, 'message' => $this->_helper->__('Número de teléfono móvil no válido.'));
        }

        return parent::saveShippingMethod($shippingMethod);
        
    }

}
