<?php
class Ecloud_EstadoAdminCustom_Model_Observer extends Mage_Core_Model_Abstract
{

    public function marcarPagado(Varien_Event_Observer $observer)
    {

        $order = $observer->getOrder();

        if ($order->getPagoBarna() != 'Si' && $order->getOrserStatus() == 'Processing') {
            $order->setPagoBarna('Si');
            $order->setFacturaBarna('Listo Para Facturar');
            $order->save();

            return;
        }
    }

    public function marcarFacturado(Varien_Event_Observer $observer)
    {

        $invoice = $observer->getInvoice();
        $order = $invoice->getOrder();
        $order->setFacturaBarna('Facturado');


        return;
    }

    public function estadoEnviado(Varien_Event_Observer $observer)
    {

        if ($observer->getShipment()->getOrder()->getShippingMethod() != "flatrate_flatrate") {
            $order = $observer->getShipment()->getOrder();
            $order->setEnvioBarna('Enviado');
        } else {
            $order = $observer->getShipment()->getOrder();
            $order->setEnvioBarna('Recogido');
        }
        return;
    }
}
