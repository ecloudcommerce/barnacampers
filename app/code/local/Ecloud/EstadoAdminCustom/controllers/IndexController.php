<?php
class Ecloud_EstadoAdminCustom_IndexController  extends Mage_Core_Controller_Front_Action
{

    public function marcarPagadoAction()
    {

        $order = Mage::getModel('sales/order')->loadByIncrementId($this->getRequest()->getParam('order_id'));

        $order->setPagoBarna('Si');
        $order->setFacturaBarna('Listo Para Facturar');
        $order->save();

        return $this->_redirectReferer();
    }

    public function enProcesoAction()
    {
        $order = Mage::getModel('sales/order')->loadByIncrementId($this->getRequest()->getParam('order_id'));

        if ($order->getEnProcesoBarna() == 'NO') {
            $order->setEnProcesoBarna('En Proceso');
            $order->save();
        }
        return $this->_redirectReferer();
    }

    public function finalizarProcesoAction()
    {
        $order = Mage::getModel('sales/order')->loadByIncrementId($this->getRequest()->getParam('order_id'));
        
        if ($order->getShippingMethod() != "flatrate_flatrate") {
            $order->setEnProcesoBarna('Finalizado');
            $order->setEnvioBarna('Listo para envio');
            $order->save();
        } else {
            $order->setEnProcesoBarna('Finalizado');
            $order->setEnvioBarna('Listo para recoger');
            $order->save();
        }
        return $this->_redirectReferer();
    }
}
