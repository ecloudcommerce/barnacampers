<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("order", "pago_barna", array("type"=>"varchar", "default"=>"NO"));
$installer->addAttribute("order", "factura_barna", array("type"=>"varchar", "default"=>"NO Listo"));
$installer->addAttribute("order", "envio_barna", array("type"=>"varchar", "default"=>"NO Listo"));
$installer->addAttribute("order", "en_proceso_barna", array("type"=>"varchar", "default"=>"NO"));
$installer->endSetup();
	 