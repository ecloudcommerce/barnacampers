<?php

require_once(Mage::getModuleDir('controllers','Mage_Contacts').DS.'IndexController.php');
 
class Ecloud_SubscribeInContact_IndexController extends Mage_Contacts_IndexController
{
    public function postAction()
    {
        $post = $this->getRequest()->getPost();
        if ($post) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                // If costumer checks newsletter option
                if (Zend_Validate::is(trim($post['is_subscribed']), 'NotEmpty')) {
                    $session            = Mage::getSingleton('core/session');
                    $customerSession    = Mage::getSingleton('customer/session');
                    $email = (string) $post['email'];
                    // Some validations may need revision
                    try {
                        if (!Zend_Validate::is($email, 'EmailAddress')) {
                            Mage::throwException($this->__('Please enter a valid email address.'));
                        }

                        if (Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG) != 1 && 
                            !$customerSession->isLoggedIn()) {
                            Mage::throwException($this->__('Sorry, but administrator denied subscription for guests. Please <a href="%s">register</a>.', Mage::helper('customer')->getRegisterUrl()));
                        }

                        $ownerId = Mage::getModel('customer/customer')
                                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                                ->loadByEmail($email)
                                ->getId();
                        if ($ownerId !== null && $ownerId != $customerSession->getId()) {
                            Mage::throwException($this->__('This email address is already assigned to another user.'));
                        }

                        $status = Mage::getModel('newsletter/subscriber')->subscribe($email);
                        if ($status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {
                            $session->addSuccess($this->__('Confirmation request has been sent.'));
                        }
                        else {
                            $session->addSuccess($this->__('Thank you for your subscription.'));
                        }
                    }
                    catch (Mage_Core_Exception $e) {
                        $session->addException($e, $this->__('There was a problem with the subscription: %s', $e->getMessage()));
                    }
                    catch (Exception $e) {
                        $session->addException($e, $this->__('There was a problem with the subscription.'));
                    }
                }
                
                if (!Zend_Validate::is(trim($post['name']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['comment']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }

                if ($error) {
                    throw new Exception();
                }
                $mailTemplate = Mage::getModel('core/email_template');
                /* @var $mailTemplate Mage_Core_Model_Email_Template */
                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->setReplyTo($post['email'])
                    ->sendTransactional(
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
                        null,
                        array('data' => $postObject)
                    );

                if (!$mailTemplate->getSentSuccess()) {
                    throw new Exception();
                }

                $translate->setTranslateInline(true);

                Mage::getSingleton('customer/session')->addSuccess(Mage::helper('contacts')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
                $this->_redirect('*/*/');

                return;
            } catch (Exception $e) {
                $translate->setTranslateInline(true);

                Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
                $this->_redirect('*/*/');
                return;
            }
        } else {
            $this->_redirect('*/*/');
        }
    }
}