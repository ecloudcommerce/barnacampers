<?php

/**
 * Product:       Xtento_EnhancedGrid (1.7.0)
 * ID:            zc/SV5tEtFi8UKC8r8I8FNXX2qtQJB3WgaciIXnJygw=
 * Packaged:      2015-07-13T16:05:27+00:00
 * Last Modified: 2013-10-05T18:01:00+02:00
 * File:          app/code/local/Xtento/EnhancedGrid/Model/Mysql4/Columns/Collection.php
 * Copyright:     Copyright (c) 2015 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_EnhancedGrid_Model_Mysql4_Columns_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('xtento_enhancedgrid/columns');
    }
}