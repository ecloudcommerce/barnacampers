<?php

/**
 * Product:       Xtento_EnhancedGrid (1.7.0)
 * ID:            zc/SV5tEtFi8UKC8r8I8FNXX2qtQJB3WgaciIXnJygw=
 * Packaged:      2015-07-13T16:05:27+00:00
 * Last Modified: 2013-10-05T17:25:29+02:00
 * File:          app/code/local/Xtento/EnhancedGrid/Model/System/Config/Source/Grid/Type.php
 * Copyright:     Copyright (c) 2015 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_EnhancedGrid_Model_System_Config_Source_Grid_Type
{
    public function toOptionArray()
    {
        return Mage::getSingleton('xtento_enhancedgrid/grid')->getTypes();
    }
}