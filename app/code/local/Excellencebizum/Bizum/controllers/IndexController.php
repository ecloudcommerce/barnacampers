<?php
/**
* NOTA SOBRE LA LICENCIA DE USO DEL SOFTWARE
* 
* El uso de este software está sujeto a las Condiciones de uso de software que
* se incluyen en el paquete en el documento "Aviso Legal.pdf". También puede
* obtener una copia en la siguiente url:
* http://www.redsys.es/wps/portal/redsys/publica/areadeserviciosweb/descargaDeDocumentacionYEjecutables
* 
* Redsys es titular de todos los derechos de propiedad intelectual e industrial
* del software.
* 
* Quedan expresamente prohibidas la reproducción, la distribución y la
* comunicación pública, incluida su modalidad de puesta a disposición con fines
* distintos a los descritos en las Condiciones de uso.
* 
* Redsys se reserva la posibilidad de ejercer las acciones legales que le
* correspondan para hacer valer sus derechos frente a cualquier infracción de
* los derechos de propiedad intelectual y/o industrial.
* 
* Redsys Servicios de Procesamiento, S.L., CIF B85955367
*/

class Excellencebizum_Bizum_IndexController extends Mage_Core_Controller_Front_Action {        
	
	public function redirectAction() {
		
		//Obtenemos los valores de la configuración del módulo
		$entorno =Mage::getStoreConfig('payment/bizum/entorno',Mage::app()->getStore());
		$nombre =Mage::getStoreConfig('payment/bizum/nombre',Mage::app()->getStore());
		$codigo =Mage::getStoreConfig('payment/bizum/num',Mage::app()->getStore());
		$clave256 =Mage::getStoreConfig('payment/bizum/clave256',Mage::app()->getStore());
		$terminal =Mage::getStoreConfig('payment/bizum/terminal',Mage::app()->getStore());
		$moneda =Mage::getStoreConfig('payment/bizum/moneda',Mage::app()->getStore());
		$trans =Mage::getStoreConfig('payment/bizum/trans',Mage::app()->getStore());
		$notif =Mage::getStoreConfig('payment/bizum/notif',Mage::app()->getStore());
		$idiomas =Mage::getStoreConfig('payment/bizum/idiomas',Mage::app()->getStore());
		$correo =Mage::getStoreConfig('payment/bizum/correo',Mage::app()->getStore());
		$mensaje =Mage::getStoreConfig('payment/bizum/mensaje',Mage::app()->getStore());
	
		//Obtenemos datos del pedido  
		$_order = new Mage_Sales_Model_Order();
		$orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
		$_order->loadByIncrementId($orderId);
		
		//Actualizamos estado del pedido a "pendiente"
		$state = 'new';
		$status = 'pending';
		$comment = 'Bizum ha actualizado el estado del pedido con el valor "'.$status.'"';
		$isCustomerNotified = true;
		$_order->setState($state, $status, $comment, $isCustomerNotified);
		$_order->save();
		//Datos del cliente
		$customer = Mage::getSingleton('customer/session')->getCustomer(); 
	
		//Datos de los productos del pedido
		$productos = '';
		$items = $_order->getAllVisibleItems();
            foreach ($items as $itemId => $item)
            { $productos .= $item->getName(); 
			  $productos .="X".$item->getQtyToInvoice(); 
			  $productos .="/";}
				
		//Formateamos el precio total del pedido
		$transaction_amount = number_format($_order->getBaseGrandTotal(),2,'', '');
		
		
				
		//Establecemos los valores del cliente y el pedido 
		$numpedido =  str_pad($orderId, 12, "0", STR_PAD_LEFT); 
		$cantidad = (float)$transaction_amount;
		$titular = $customer->getFirstname()." ".$customer->getMastname()." ".$customer->getLastname();  

		//Generamos el urlTienda -> respuesta ON-LINE que deberá ser la establecida bajo las pautas de WooCommerce
		$urltienda = Mage::getBaseUrl().'bizum/index/notify';
		
		// Obtenemos el valor de la config del idioma
		if($idiomas=="0"){
			$idioma_tpv="0";
		}
		else { 
			$idioma_web = substr(Mage::getStoreConfig('general/locale/code', Mage::app()->getStore()->getId()),0,2);
			switch ($idioma_web) {
				case 'es':
				$idioma_tpv='001';
				break;
				case 'en':
				$idioma_tpv='002';
				break;
				case 'ca':
				$idioma_tpv='003';
				break;
				case 'fr':
				$idioma_tpv='004';
				break;
				case 'de':
				$idioma_tpv='005';
				break;
				case 'nl':
				$idioma_tpv='006';
				break;
				case 'it':
				$idioma_tpv='007';
				break;
				case 'sv':
				$idioma_tpv='008';
				break;
				case 'pt':
				$idioma_tpv='009';
				break;
				case 'pl':
				$idioma_tpv='011';
				break;
				case 'gl':
				$idioma_tpv='012';
				break;
				case 'eu':
				$idioma_tpv='013';
				break;
				default:
				$idioma_tpv='002';
			}		
		}
		
		// Obtenemos el código ISO del tipo de moneda
		if($moneda=="0"){
			$moneda="978";
		} else { 
			$moneda="840";
		}

        $urlOK = Mage::getBaseUrl() . 'redsys/index/success';
        $urlKO = Mage::getBaseUrl() . 'redsys/index/cancel';

		$bizumApi = Mage::helper('excellencebizum_bizum/api');
		$bizumApi->setParameter("DS_MERCHANT_AMOUNT",$cantidad);
		$bizumApi->setParameter("DS_MERCHANT_ORDER",strval($numpedido));
		$bizumApi->setParameter("DS_MERCHANT_MERCHANTCODE",$codigo);
		$bizumApi->setParameter("DS_MERCHANT_CURRENCY",$moneda);
		$bizumApi->setParameter("DS_MERCHANT_TRANSACTIONTYPE",$trans);
		$bizumApi->setParameter("DS_MERCHANT_TERMINAL",$terminal);
		$bizumApi->setParameter("DS_MERCHANT_MERCHANTURL",$urltienda);
		$bizumApi->setParameter("DS_MERCHANT_URLOK",$urlOK);
		$bizumApi->setParameter("DS_MERCHANT_URLKO",$urlKO);
		$bizumApi->setParameter("Ds_Merchant_ConsumerLanguage",$idioma_tpv);
		$bizumApi->setParameter("Ds_Merchant_ProductDescription",$productos);
		$bizumApi->setParameter("Ds_Merchant_Titular",$titular);
		$bizumApi->setParameter("Ds_Merchant_MerchantData",sha1($urltienda));
		$bizumApi->setParameter("Ds_Merchant_MerchantName",$nombre);
		$bizumApi->setParameter("Ds_Merchant_PayMethods",'z');
		$bizumApi->setParameter("Ds_Merchant_Module","magento_bizum_3.0.2");

		//Datos de configuración
		$bizumLib = Mage::helper("excellencebizum_bizum/lib");
		$version = $bizumLib->getVersionClave();
		
		//Clave del comercio que se extrae de la configuración del comercio
		// Se generan los parámetros de la petición
		$request = "";
		$paramsBase64 = $bizumApi->createMerchantParameters();
		$signatureMac = $bizumApi->createMerchantSignature($clave256);

		// Obtenemos el valor de la consulta para saber el entorno del TPV.
		if($entorno=="1"){
		echo ('<form action="http://sis-d.redsys.es/sis/realizarPago/utf-8" method="post" id="bizum_form" name="bizum_form">');}
		else if($entorno=="2"){
		echo ('<form action="https://sis-i.redsys.es:25443/sis/realizarPago/utf-8" method="post" id="bizum_form" name="bizum_form">');}
		else if($entorno=="3"){
		echo ('<form action="https://sis-t.redsys.es:25443/sis/realizarPago/utf-8" method="post" id="bizum_form" name="bizum_form">');}	
		else{
		echo ('<form action="https://sis.redsys.es/sis/realizarPago/utf-8" method="post" id="bizum_form" name="bizum_form">');}	
		
		// Establecemos el valor de los input
		echo ('
				<input type="hidden" name="Ds_SignatureVersion" value="'.$version.'" />
				<input type="hidden" name="Ds_MerchantParameters" value="'.$paramsBase64.'" />
				<input type="hidden" name="Ds_Signature" value="'.$signatureMac.'" />
				</form>
			
				<h3> Cargando el TPV... Espere por favor. </h3>		
				
				<script type="text/javascript" data-cfasync="false">
					document.bizum_form.submit();
				</script>'
				);

	}

	
    public function notifyAction()
    {
		//header( 'Content-Type:text/html; charset=UTF-8' );

    	$bizumLib=Mage::helper("excellencebizum_bizum/lib");
    	$idLog = $bizumLib->generateIdLog();
		$logActivo =Mage::getStoreConfig('payment/bizum/logactivo',Mage::app()->getStore());
		$mantenerPedidoAnteError =Mage::getStoreConfig('payment/bizum/errorpago',Mage::app()->getStore());
		$orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
		$estado =Mage::getStoreConfig('payment/bizum/estado',Mage::app()->getStore());

		if($estado==="")
			$estado="Processing";

        if (!empty($_POST)) //URL RESP. ONLINE
        { 
			/** Recoger datos de respuesta **/
			$version     = $_POST["Ds_SignatureVersion"];
			$datos    = $_POST["Ds_MerchantParameters"];
			$firma_remota    = $_POST["Ds_Signature"];

			// Se crea Objeto
			$bizumApi = Mage::helper("excellencebizum_bizum/api");
			
			/** Se decodifican los datos enviados y se carga el array de datos **/
			$decodec = $bizumApi->decodeMerchantParameters($datos);

			/** Clave **/
			$kc = Mage::getStoreConfig('payment/bizum/clave256',Mage::app()->getStore());
			
			/** Se calcula la firma **/
			$firma_local = $bizumApi->createMerchantSignatureNotif($kc,$datos);
			
			/** Extraer datos de la notificación **/
			$total     = $bizumApi->getParameter('Ds_Amount');
			$pedido    = $bizumApi->getParameter('Ds_Order');
			$codigo    = $bizumApi->getParameter('Ds_MerchantCode');
			$terminal  = $bizumApi->getParameter('Ds_Terminal');
			$moneda    = $bizumApi->getParameter('Ds_Currency');
			$respuesta = $bizumApi->getParameter('Ds_Response');
			$fecha	   = $bizumApi->getParameter('Ds_Date');
			$hora	   = $bizumApi->getParameter('Ds_Hour');
			$id_trans  = $bizumApi->getParameter('Ds_AuthorisationCode');
			$tipoTrans = $bizumApi->getParameter('Ds_TransactionType');

			// Recogemos los datos del comercio
			$codigoOrig = Mage::getStoreConfig('payment/bizum/num',Mage::app()->getStore());	
			$terminalOrig = Mage::getStoreConfig('payment/bizum/terminal',Mage::app()->getStore());
			$monedaOrig =Mage::getStoreConfig('payment/bizum/moneda',Mage::app()->getStore());
			$tipoTransOrig =Mage::getStoreConfig('payment/bizum/trans',Mage::app()->getStore());

			// Obtenemos el código ISO del tipo de moneda
			if($monedaOrig == "0"){
				$monedaOrig = "978";
			} else { 
				$monedaOrig = "840";
			}

			// Inicializamos el valor del status del pedido
			$status="";

			if(!($firma_local === $firma_remota)){
				$this->escribirLog($idLog." -- Validacion de firma",$logActivo);
				$this->escribirLog($idLog." -- "."KC  '".$kc."'",$logActivo);
				$this->escribirLog($idLog." -- "."Firma local  '".$firma_local."'",$logActivo);
				$this->escribirLog($idLog." -- "."Firma remota '".$firma_remota."'",$logActivo);
			}
			if(!$bizumLib->checkImporte($total)){
				$this->escribirLog($idLog." -- Validacion de importe '".$total."'",$logActivo);
			}
			if(!$bizumLib->checkPedidoNum($pedido)){
				$this->escribirLog($idLog." -- Validacion de pedido '".$pedido."'",$logActivo);
			}
			if(!$bizumLib->checkFuc($codigo)){
				$this->escribirLog($idLog." -- Validacion de fuc '".$codigo."'",$logActivo);
			}
			if(!$bizumLib->checkMoneda($moneda)){
				$this->escribirLog($idLog." -- Validacion de moneda '".$moneda."'",$logActivo);
			}
			if(!$bizumLib->checkRespuesta($respuesta)){
				$this->escribirLog($idLog." -- Validacion de respuesta '".$respuesta."'",$logActivo);
			}
			if(!$bizumLib->checkRespuesta($respuesta)){
				$this->escribirLog($idLog." -- Validacion de respuesta '".$respuesta."'",$logActivo);
			}
			if(!($codigo == $codigoOrig)){
				$this->escribirLog($idLog." -- Validacion de fuc original '".$codigo."' '".$codigoOrig."'",$logActivo);
			}
			if(!($terminalOrig == $terminal)){
				$this->escribirLog($idLog." -- Validacion de terminal '".$terminalOrig."' '".$terminal."'",$logActivo);
			}
			
			// Validacion de firma y parámetros
			if ($firma_local === $firma_remota
					&& $bizumLib->checkImporte($total)					
					&& $bizumLib->checkPedidoNum($pedido)
					&& $bizumLib->checkFuc($codigo)
					&& $bizumLib->checkMoneda($moneda)
					&& $bizumLib->checkRespuesta($respuesta)
					&& $tipoTrans == $tipoTransOrig
					&& $codigo == $codigoOrig
					&& intval(strval($terminalOrig)) == intval(strval($terminal))
				) {
					// Respuesta cumple las validaciones
					$respuesta = intval($respuesta);
					$this->escribirLog($idLog." - Código de respuesta: ".$respuesta,$logActivo);
					if ($respuesta < 101){
							$this->escribirLog($idLog." - Pago aceptado.",$logActivo);
							//Correo electrónico
							$correo = Mage::getStoreConfig('payment/bizum/correo',Mage::app()->getStore());
							$mensaje = Mage::getStoreConfig('payment/bizum/mensaje',Mage::app()->getStore());
							$nombreComercio = Mage::getStoreConfig('payment/bizum/nombre',Mage::app()->getStore());
							//Datos del cliente
							$customer = Mage::getSingleton('customer/session')->getCustomer();
							if($correo!="0"){
							    $email_to = $customer->getEmail();
								$email_subject = "-MAGENTO- ".$this->__("Pedido realizado");
								if(mail($email_to, $email_subject, $mensaje,"From:".$nombreComercio)){
									echo "Correo enviado";
								} else {
									echo "No se puedo enviar el correo";
								}
							}
							//Fin de correo
							
							//Id pedido
							$orde    = intval($pedido);
							$order = Mage::getModel('sales/order')->loadByIncrementId($orde);
							
							$transaction_amount = number_format($order->getBaseGrandTotal(),2,'', '');
							$amountOrig = (float)$transaction_amount;
							
							if ($amountOrig != $total) {
								$this->escribirLog($idLog." -- "."El importe total($amountOrig) no coincide con el recibido desde el servidor($total)",$logActivo);
								// Diferente importe
								$state = 'new';
								$status = 'canceled';
								$comment = $this->__("El importe total($amountOrig) no coincide con el recibido desde el servidor($total).");
								$isCustomerNotified = false;
								$order->setState($state, $status, $comment, $isCustomerNotified);
								$order->registerCancellation("")->save();
								$order->save();
								$this->escribirLog($idLog." -- "."El pedido con ID de carrito " . $orde . " es inválido.",$logActivo);
								die($this->__("Validaciones NO superadas"));
							}

							try {
								if(!$order->canInvoice()) {
									$order->addStatusHistoryComment($this->__("Bizum, imposible generar Factura."), false);
									$order->save();
								}
								//START Handle Invoice
								$invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
								$invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
								$invoice->register();
								$invoice->getOrder()->setCustomerNoteNotify(true);
								$order->addStatusHistoryComment('Bizum ha generado la Factura del pedido', false);
								$transactionSave = Mage::getModel('core/resource_transaction')
									->addObject($invoice)
									->addObject($invoice->getOrder());
								$transactionSave->save();
								//END Handle Invoice
								
								//START Handle Shipment
								//$shipment = $order->prepareShipment();
								//$shipment->register();
								//$order->setIsInProcess(true);
								//$order->addStatusHistoryComment('Bizum ENVIO.', false);
								//$transactionSave = Mage::getModel('core/resource_transaction')
								//	->addObject($shipment)
								//	->addObject($shipment->getOrder())
								//	->save();
								//END Handle Shipment
								
								//Email al cliente
								$order->sendNewOrderEmail();
							
								//Se actualiza el pedido
								$state = 'new';
								$comment = $this->__("Bizum ha actualizado el estado del pedido con el valor").' "'.$estado.'"';
								$isCustomerNotified = true;
								$order->setState($state, $estado, $comment, $isCustomerNotified);
								$order->save();
								$this->escribirLog($idLog." -- "."El pedido con ID de carrito " . $orderId . " es válido y se ha registrado correctamente.",$logActivo);
								die($this->__("Validaciones superadas"));
								
							} catch (Exception $e) {
								$this->escribirLog($idLog." -- "."Excepción: $e ",$logActivo);
								$order->addStatusHistoryComment('Bizum: Exception message: '.$e->getMessage(), false);
								$order->save();
							}
					}
					else {
							$this->escribirLog($idLog." - Pago no aceptado",$logActivo);
							$orde    = intval($pedido);
							$order = Mage::getModel('sales/order')->loadByIncrementId($orde);
							$state = 'new';
							$status = 'canceled';
							$comment = $bizumLib->textDsResponse($respuesta);
							$this->escribirLog($idLog." - Actualizado el estado del pedido con el valor ".$status,$logActivo);
							$isCustomerNotified = false;
							$order->setState($state, $status, $comment, $isCustomerNotified);
							$order->registerCancellation("")->save();
							$order->save();
							die($this->__("Validaciones NO superadas"));
					}
				
				}// if (firma_local=firma_remota)
				else {
					$this->escribirLog($idLog." - Validaciones NO superadas",$logActivo);
					$orde    = intval($pedido);
					$order = Mage::getModel('sales/order')->loadByIncrementId($orde);
					$state = 'new';
					$status = 'canceled';
					$comment = $this->__("Validaciones NO superadas").' "'.$status.'"';
					$isCustomerNotified = true;
					$order->setState($state, $status, $comment, $isCustomerNotified);
					$order->registerCancellation("")->save();
					$order->save();
					die("Validaciones NO superadas");
				}
		
		
        } 
		else if (!empty($_GET)) //URL OK Y KO
        { 
			/** Recoger datos de respuesta **/
			$version     = $_GET["Ds_SignatureVersion"];
			$datos    = $_GET["Ds_MerchantParameters"];
			$firma_remota    = $_GET["Ds_Signature"];
			
			// Se crea Objeto
			$bizumApi = Mage::helper("excellencebizum_bizum/api");
			
			/** Se decodifican los datos enviados y se carga el array de datos **/
			$decodec = $bizumApi->decodeMerchantParameters($datos);

			/** Clave **/
			$kc = Mage::getStoreConfig('payment/bizum/clave256',Mage::app()->getStore());
			
			/** Se calcula la firma **/
			$firma_local = $bizumApi->createMerchantSignatureNotif($kc,$datos);
			
			/** Extraer datos de la notificación **/
			$total     = $bizumApi->getParameter('Ds_Amount');
			$pedido    = $bizumApi->getParameter('Ds_Order');
			$codigo    = $bizumApi->getParameter('Ds_MerchantCode');
			$terminal  = $bizumApi->getParameter('Ds_Terminal');
			$moneda    = $bizumApi->getParameter('Ds_Currency');
			$respuesta = $bizumApi->getParameter('Ds_Response');
			$fecha	   = $bizumApi->getParameter('Ds_Date');
			$hora	   = $bizumApi->getParameter('Ds_Hour');
			$id_trans  = $bizumApi->getParameter('Ds_AuthorisationCode');
			$tipoTrans = $bizumApi->getParameter('Ds_TransactionType');

			// Recogemos los datos del comercio
			$codigoOrig = Mage::getStoreConfig('payment/bizum/num',Mage::app()->getStore());	
			$terminalOrig = Mage::getStoreConfig('payment/bizum/terminal',Mage::app()->getStore());
			$monedaOrig =Mage::getStoreConfig('payment/bizum/moneda',Mage::app()->getStore());
			$tipoTransOrig =Mage::getStoreConfig('payment/bizum/trans',Mage::app()->getStore());

			// Obtenemos el código ISO del tipo de moneda
			if($monedaOrig == "0"){
				$monedaOrig = "978";
			} else { 
				$monedaOrig = "840";
			}

			if ($firma_local === $firma_remota
					&& $bizumLib->checkImporte($total)					
					&& $bizumLib->checkPedidoNum($pedido)
					&& $bizumLib->checkFuc($codigo)
					&& $bizumLib->checkMoneda($moneda)
					&& $bizumLib->checkRespuesta($respuesta)
					&& $tipoTrans == $tipoTransOrig
					&& $codigo == $codigoOrig
					&& intval(strval($terminalOrig)) == intval(strval($terminal))
				) {
					$respuesta = intval($respuesta);
					$orde = intval($pedido);
					$order = Mage::getModel('sales/order')->loadByIncrementId($orde);
					if ($respuesta < 101){
						$transaction_amount = number_format($order->getBaseGrandTotal(),2,'', '');
						$amountOrig = (float)$transaction_amount;
						if ($amountOrig != $total) {
							$this->redirigeError($mantenerPedidoAnteError);
							$this->_redirect('checkout/onepage/failure');
						} else {
							$this->_redirect('checkout/onepage/success');
						}
					} else {
						$this->redirigeError($mantenerPedidoAnteError);
					}
				} else {
					$this->redirigeError($mantenerPedidoAnteError);
				}
        } else echo $this->__("No hay respuesta por parte de Bizum!");
	}
	
	private function redirigeError($mantenerPedido){
		if(strval($mantenerPedido) == 1) {
			$_order = new Mage_Sales_Model_Order();
			$orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
			$_order->loadByIncrementId($orderId);
			
			$quote = Mage::getModel('sales/quote')->load($_order->getQuoteId());
			$quote->getBillingAddress()->setSaveInAddressBook(0);
			$quote->getShippingAddress()->setSaveInAddressBook(0);
			if ($quote->getId()) {
				$quote->setIsActive(1)
					->setReservedOrderId(null)
					->save();
				Mage::getSingleton('checkout/session')
					->replaceQuote($quote)
					->unsLastRealOrderId();
			}
		}
		$this->_redirect('checkout/onepage/failure');
	}
	
    public function indexAction() {

        echo 'Hello MAGENTO!';

    }

	public function escribirLog($texto,$activo) {
		if($activo==1){
			Mage::log('>>>> Bizum: '.$texto);
		}
	}

}


?>