<?php


/**
 * Description of Data
 *
 * @author davidslater
 */
class Interactiv4_EventLogger_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     *
     * @param string $processName
     * @param string $message 
     * @return Interactiv4_EventLogger_Helper_Data
     */
    public function log($processName, $message) {
        $log = Mage::getModel('i4eventlogger/log'); /* @var $log Interactiv4_EventLogger_Model_Log */
        $log->setProcessName($processName)
                ->setMessage($message)
                ->save();
        return $this;
    }
}

?>
