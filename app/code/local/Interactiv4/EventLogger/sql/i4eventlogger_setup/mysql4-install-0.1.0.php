<?php
/**
 * Interactiv4_EventLogger Module for Magento
 *
 * @category   Interactiv4
 * @package    Interactiv4_EventLogger
 */

$installer = $this;

$installer->startSetup();

$installer->run("
CREATE TABLE  {$this->getTable('i4eventlogger/log')} (
`i4eventlogger_log_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`log_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`process_name` VARCHAR( 50 ) NULL ,
`message` TEXT NULL ,
INDEX (  `log_date` ,  `process_name` )
) ENGINE = INNODB;

    ");

$installer->endSetup(); 