<?php

class Interactiv4_EventLogger_Block_Adminhtml_Log_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    /**
     *
     * @var array 
     */
    protected $_map = null;

    public function __construct() {
        parent::__construct();
        $this->setId('i4eventlogger_logGrid');
        $this->setDefaultSort('i4eventlogger_log_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('i4eventlogger/log')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {


        $this->addColumn('i4eventlogger_log_id', array(
            'header' => $this->_getHelper()->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'i4eventlogger_log_id',
        ));


        $this->addColumn('log_date', array(
            'header' => $this->_getHelper()->__('Date'),
            'index' => 'log_date',
            'default' => '',
            'type' => 'date'
        ));


        $this->addColumn('process_name', array(
            'header' => $this->_getHelper()->__('Process Name'),
            'align' => 'left',
            'index' => 'process_name',
            'type' => 'text',
        ));

        $this->addColumn('message', array(
            'header' => $this->_getHelper()->__('Message'),
            'align' => 'left',
            'index' => 'message',
            'type' => 'text',
            'renderer' => 'Interactiv4_EventLogger_Block_Adminhtml_Log_Grid_Renderer_Message',
            'default' => '*',
        ));


        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('i4eventlogger_log_id');
        $this->getMassactionBlock()->setFormFieldName('i4eventlogger_log_id');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->_getHelper()->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => $this->_getHelper()->__('Are you sure?')
        ));
        return $this;
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }


    /**
     *
     * @return Interactiv4_EventLogger_Helper_Data 
     */
    protected function _getHelper() {
        return Mage::helper('i4tablerates');
    }

}