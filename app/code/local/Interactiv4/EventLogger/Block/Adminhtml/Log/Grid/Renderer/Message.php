<?php

/**
 * Description of Message
 *
 * @author davidslater
 */
class Interactiv4_EventLogger_Block_Adminhtml_Log_Grid_Renderer_Message extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $value = $row->getData($this->getColumn()->getIndex());
        $value = preg_replace("/#ORDER_ID_URL=(\d+)#/", Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/view', array('order_id' => '${1}')), $value);
        $value = preg_replace("/#SHIPMENT_ID_URL=(\d+)#/", Mage::helper('adminhtml')->getUrl('adminhtml/sales_shipment/view', array('shipment_id' => '${1}')), $value);
        return $value;
    }

    /**
     *
     * @param mixed $entity
     * @param int $id
     * @return string 
     */
    public static function getUrlPlaceHolder($entity) {
        if ($entity instanceof Mage_Sales_Model_Order) {
            return "#ORDER_ID_URL={$entity->getId()}#";
        } elseif ($entity instanceof Mage_Sales_Model_Order_Shipment) {
            return "#SHIPMENT_ID_URL={$entity->getId()}#";
        } else {
            return "#";
        }
    }

}

?>
