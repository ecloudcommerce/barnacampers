<?php

class Interactiv4_EventLogger_Block_Adminhtml_Log extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_log';
        $this->_blockGroup = 'i4eventlogger';
        $this->_headerText = $this->_getHelper()->__("Interactiv4 Event Log");



        $this->_addButton('i4exportcsv', array(
            'label' => $this->_getHelper()->__('Export CSV'),
            'onclick' => "setLocation('{$this->getUrl('*/*/exportCsv')}')"
        ));

        $this->_addButton('i4exportexcel', array(
            'label' => $this->_getHelper()->__('Export Excel'),
            'onclick' => "setLocation('{$this->getUrl('*/*/exportExcel')}')"
        ));
        parent::__construct();

        $this->_removeButton('add');
    }

    /**
     *
     * @return Interactiv4_EventLogger_Helper_Data 
     */
    protected function _getHelper() {
        return Mage::helper('i4eventlogger');
    }

}
