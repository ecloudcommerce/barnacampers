<?php

/**
 * Interactiv4_EventLogger Module for Magento
 *
 * @category   Interactiv4
 * @package    Interactiv4_EventLogger
 */
class Interactiv4_EventLogger_Adminhtml_LogController extends Mage_Adminhtml_Controller_Action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('system/i4eventlogger/logs');
        return $this;
    }

    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }


    public function exportCsvAction()
    {
        $fileName   = 'i4eventlogger.csv';
        $grid       = $this->getLayout()->createBlock('i4eventlogger/adminhtml_log_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }


    public function exportExcelAction()
    {
        $fileName   = 'i4eventlogger.xml';
        $grid       = $this->getLayout()->createBlock('i4eventlogger/adminhtml_log_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }




    public function massDeleteAction() {
        $eventloggerLogIds = $this->getRequest()->getParam('i4eventlogger_log_id');
        if (!is_array($eventloggerLogIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($eventloggerLogIds as $eventloggerLogId) {
                    $eventLoggerLog = Mage::getModel('i4eventlogger/log')->load($eventloggerLogId);
                    $eventLoggerLog->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('adminhtml')->__(
                                'Total of %d record(s) were successfully deleted', count($eventloggerLogIds)
                        )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
    
    public function gridAction() {

        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('i4eventlogger/adminhtml_log_grid')->toHtml()
        );
    }    



}