<?php

class Interactiv4_PrintShippingLabel_Block_Adminhtml_Sales_View extends Mage_Adminhtml_Block_Sales_Order_View {

    public function __construct() {
        parent::__construct();
        if ($this->_hasPdfShippingLabels()) {
            $this->_addButton('i4print_shipping_label', array(
                'label' => $this->_getHelper()->__('Print Shipping Labels'),
                'onclick' => "setLocation('{$this->_getPrintShippingLabelsUrl()}')"
            ));
        }
    }

    /**
     *
     * @return boolean 
     */
    protected function _hasPdfShippingLabels() {
        $shipments = $this->getOrder()->getShipmentsCollection(); /* @var $shipments Mage_Sales_Model_Resource_Order_Shipment_Collection */
        $shipments->addFieldToFilter('shipping_label', array("notnull" => true));
        return $shipments->getSize() > 0;
    }

    /**
     *
     * @return string 
     */
    protected function _getPrintShippingLabelsUrl() {
        return Mage::helper('adminhtml')->getUrl('i4printshippinglabel/adminhtml_index/printshippinglabels', array('order_id' => $this->getOrder()->getId()));
    }

    /**
     *
     * @return Interactiv4_PrintShippingLabel_Helper_Data 
     */
    protected function _getHelper() {
        return Mage::helper('i4printshippinglabel');
    }

}
