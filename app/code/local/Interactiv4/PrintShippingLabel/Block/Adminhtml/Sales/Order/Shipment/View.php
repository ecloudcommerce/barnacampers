<?php

/**
 * Description of View
 *
 * @author davidslater
 */
class Interactiv4_PrintShippingLabel_Block_Adminhtml_Sales_Order_Shipment_View extends Mage_Adminhtml_Block_Sales_Order_Shipment_View {

    public function __construct() {
        parent::__construct();
        if ($this->_hasPdfShippingLabel()) {
            $this->_addButton('i4print_shipping_label', array(
                'label' => $this->_getHelper()->__('Print Shipping Labels'),
                'onclick' => "setLocation('{$this->_getPrintShippingLabelsUrl()}')"
            ));
        }
    }

    /**
     *
     * @return boolean 
     */
    protected function _hasPdfShippingLabel() {
        return $this->getShipment()->getShippingLabel() ? true : false;
    }

    /**
     *
     * @return string 
     */
    protected function _getPrintShippingLabelsUrl() {
        return Mage::helper('adminhtml')->getUrl('i4printshippinglabel/adminhtml_index/printshippinglabels', array('shipment_id' => $this->getShipment()->getId()));
    }

    /**
     *
     * @return Interactiv4_PrintShippingLabel_Helper_Data 
     */
    protected function _getHelper() {
        return Mage::helper('i4printshippinglabel');
    }

}

?>
