<?php

/**
 * Description of IndexController
 *
 * @author davidslater
 */
class Interactiv4_PrintShippingLabel_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action {

    public function printshippinglabelsAction() {
        $module = (string) Mage::getConfig()->getNode('admin/routers/adminhtml/args/frontName');
        $controller = 'sales_order_shipment';
        $action = 'massPrintShippingLabel';
        $orderId = $this->getRequest()->getParam('order_id');
        if ($orderId) {
            $this->getRequest()->setPost('order_ids', array($orderId));
            $this->getRequest()->setParam('massaction_prepare_key', 'order_ids');
            $this->_forward($action, $controller, $module);
            return;
        } 
        
        $shipmentId = $this->getRequest()->getParam('shipment_id');
        if ($shipmentId) {
            $this->getRequest()->setPost('shipment_ids', array($shipmentId));
            $this->getRequest()->setParam('massaction_prepare_key', 'shipment_ids');
            $this->_forward($action, $controller, $module);
            return;            
        }
        $this->_redirect('adminhtml/sales_order/index');
    }
	
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales');
    }

}

?>
