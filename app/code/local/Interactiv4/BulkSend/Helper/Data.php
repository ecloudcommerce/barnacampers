<?php

/**
 * Description of Data
 *
 * @author davidslater
 */
class Interactiv4_BulkSend_Helper_Data extends Mage_Core_Helper_Abstract {

    const MAX_TRIES_FOR_BULK_SEND = 5;

    public function carrierSupportsBulkSend(Mage_Shipping_Model_Carrier_Abstract $carrier = null) {
        if (!$carrier) {
            return false;
        }
        if (!$this->_getPackedShipmentHelper()->carrierSupportsPackedShipment($carrier)) {
            return false;
        }
        if (!$carrier->getConfigData("i4bulksend/supports_bulk_send")) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param Mage_Sales_Model_Order $order
     * @return int 
     */
    public function getOrderBulkSendCounter(Mage_Sales_Model_Order $order) {
        return $order->getData('i4bulksend');
    }

    /**
     *
     * @param Mage_Sales_Model_Order $order
     * @param type $value
     * @return \Mage_Sales_Model_Order 
     */
    public function setOrderBulkSendCount(Mage_Sales_Model_Order $order, $value, $save = true) {
        $order->setData('i4bulksend', $value);
        if ($save) {
            $order->save();
        }
        return $order;
    }

    /**
     *
     * @param Mage_Sales_Model_Order $order
     * @param boolean $save
     * @return boolean 
     */
    public function incrementOrderBulkSendCounter(Mage_Sales_Model_Order $order, $save = true) {
        $currentCount = $this->getOrderBulkSendCounter($order);
        if ($currentCount < self::MAX_TRIES_FOR_BULK_SEND) {
            $currentCount++;
            $this->setOrderBulkSendCount($order, $currentCount, $save);
            $success = true;
        } else {
            $this->setOrderBulkSendCount($order, -1, $save);
            $success = false;
        }
        return $success;
    }

    /**
     *
     * @param Mage_Sales_Model_Order $order
     * @return \Mage_Sales_Model_Order 
     */
    public function clearOrderBulkSendCounter(Mage_Sales_Model_Order $order, $save = true) {
        $this->setOrderBulkSendCount($order, 0, $save);
        return $order;
    }

    /**
     *
     * @param Mage_Sales_Model_Order $order
     * @return boolean 
     */
    public function bulkSendFailedForOrder(Mage_Sales_Model_Order $order) {
        return $this->getOrderBulkSendCounter($order) < 0;
    }

    /**
     *
     * @param Mage_Sales_Model_Order $order
     * @return boolean 
     */
    public function isPendingBulkSend(Mage_Sales_Model_Order $order) {
        return $this->getOrderBulkSendCounter($order) > 0;
    }

    /**
     *
     * @param string $message
     * @param type $eventlogger
     * @param type $processName
     * @param type $method
     * @param type $line
     * @return string 
     */
    public function log($message, $eventlogger = true, $processName = 'Bulk Send', $method = '', $line = '') {
        if ($eventlogger) {
            try {
                $this->_getEventLoggerHelper()->log($processName, $message);
            } catch (Exception $e) {
                Mage::log($this->__("Failed to log message '%s' from process '%s' in the event log. Exception '%s'", $message, $processName, $e->getMessage()));
            }
        }
        $message = $message . ( $method ? " in $method" : "") . ($line ? " on line $line" : '');
        Mage::log($message, null, 'i4bulksend.log');
        return $message;
    }

    /**
     *
     * @return Interactiv4_PackedShipment_Helper_Data 
     */
    protected function _getPackedShipmentHelper() {
        return Mage::helper('i4packedshipment');
    }

    /**
     *
     * @return Interactiv4_EventLogger_Helper_Data 
     */
    protected function _getEventLoggerHelper() {
        return Mage::helper('i4eventlogger');
    }

}

?>
