<?php

/**
 * Description of Observer
 *
 * @author davidslater
 */
class Interactiv4_BulkSend_Model_Observer  {
    public function addSendInOnePackageAction(Varien_Event_Observer $observer) {
        
        $block = $observer->getEvent()->getBlock(); /* @var $block Mage_Adminhtml_Block_Abstract */
        
        if ($block instanceof Mage_Adminhtml_Block_Widget_Grid_Massaction
                && $block->getRequest()->getControllerName() == 'sales_order') {
            $block->addItem('send_in_one_packet', array(
                'label' => $this->_getHelper()->__('Send In One Packet'),
                'url' => Mage::helper("adminhtml")->getUrl('i4bulksend/adminhtml_index/index'),
            ));
        }        
    }
    
    /**
     *
     * @return Interactiv4_BulkSend_Helper_Data 
     */
    protected function _getHelper() {
        return Mage::helper('i4bulksend');
    }
}

?>
