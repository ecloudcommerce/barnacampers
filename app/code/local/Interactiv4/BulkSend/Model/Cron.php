<?php

/**
 * Description of Cron
 *
 * @author davidslater
 */
class Interactiv4_BulkSend_Model_Cron {

    public function sendQueuedOrdersInOnePackage(Mage_Cron_Model_Schedule $schedule = null, $manualInvocation = false) {
        $queuedOrders = Mage::getResourceModel('sales/order_collection'); /* @var $queuedOrders Mage_Sales_Model_Resource_Order_Collection */
        $queuedOrders->addFieldToFilter("i4bulksend", array("gt" => 0));
        
        if ($queuedOrders->getSize() == 0) {
            return;
        }

        if ($manualInvocation) {
            $this->_getHelper()->log($this->_getHelper()->__("Started bulk send of orders in one package using manual invocation."));
        } else {
            $this->_getHelper()->log($this->_getHelper()->__("Started scheduled bulk send of orders in one package."));
        }

        try {



            foreach ($queuedOrders as $order) { /* @var $order Mage_Sales_Model_Order */
                try {
                    if (!$this->_getHelper()->carrierSupportsBulkSend($order->getShippingCarrier())) {
                        $this->_logOrderMessage("Order %s cannot be sent within the bulk send process because the shipping carrier does not support that option.", $order);
                        $this->_getHelper()->clearOrderBulkSendCounter($order);
                        continue;
                    }


                    if (!$order->canShip()) {
                        throw new Exception("The order cannot be shipped at this time. Perhaps all of the shipments are already completed.");
                    }

                    $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment(array()); /* @var $shipment Mage_Sales_Model_Order_Shipment */
                    $shipment->register();
                    $this->_getPackedShipmentHelper()->registerBulkSend(true);
                    $order->setIsInProcess(true);
                    
                    $transactionSave = Mage::getModel('core/resource_transaction')
                            ->addObject($shipment)
                            ->addObject($order);
                    $transactionSave->save();

                    $this->_logOrderMessage("A shipment was successfully created for order %s and communicated to the shipping carrier as one package. The shipment reference is %s.", $order, $this->_getShipmentUrlHtml($shipment));
                    $comment = $this->_getHelper()->__("The shipment %s was created for this order and it was successfully communicated to the shipping carrier as one package.", $shipment->getIncrementId());
                    $order->addStatusHistoryComment($comment);
                    $order->save();
                } catch (Exception $e) {
                    if ($this->_getHelper()->incrementOrderBulkSendCounter($order)) {
                        $this->_logOrderMessage("A problem occurred attempting to send order %s in one package. The shipment will be re-tried on the next pass. Error message: '%s'.", $order, $e->getMessage());
                    } else {
                        $this->_logOrderMessage("A problem occurred attempting to send order %s in one package. No further attempts will be made to ship the order through the bulk send process. Please try to create the shipment for this order manually. Error message: '%s'.", $order, $e->getMessage());
                    }
                }
            }
        } catch (Exception $e) {
            $this->_getHelper()->log($this->_getHelper()->__("An error occurred whilst processing orders for sending in one package and the process was terminated unsuccessfully: '%s'", $e->getMessage()));
        }

        $this->_getHelper()->log($this->_getHelper()->__("Finished bulk send of orders in one package."));
    }

    /**
     *
     * @param string $message
     * @param Mage_Sales_Model_Order $order
     * @return string 
     */
    protected function _logOrderMessage($message, Mage_Sales_Model_Order $order, $additional = '') {
        $orderLink = '<a href="' . $this->_getUrlPlaceholder($order) . '">' . $order->getIncrementId() . '</a>';
        if ($additional) {
            return $this->_getHelper()->log($this->_getHelper()->__($message, $orderLink, $additional));
        } else {
            return $this->_getHelper()->log($this->_getHelper()->__($message, $orderLink));
        }
    }

    /**
     *
     * @param mixed $entity
     * @return string 
     */
    protected function _getUrlPlaceholder($entity) {
        return Interactiv4_EventLogger_Block_Adminhtml_Log_Grid_Renderer_Message::getUrlPlaceHolder($entity);
    }

    /**
     *
     * @return Interactiv4_PackedShipment_Helper_Data 
     */
    protected function _getPackedShipmentHelper() {
        return Mage::helper('i4packedshipment');
    }

    /**
     *
     * @param Mage_Sales_Model_Order_Shipment $shipment
     * @return string 
     */
    protected function _getShipmentUrlHtml(Mage_Sales_Model_Order_Shipment $shipment) {
        $url = $this->_getUrlPlaceholder($shipment);
        return '<a href="' . $url . '">' . $shipment->getIncrementId() . '</a>';
    }

    //protected function _getUrlPlaceholder($entity, $id)

    /**
     *
     * @return Interactiv4_BulkSend_Helper_Data 
     */
    protected function _getHelper() {
        return Mage::helper('i4bulksend');
    }

}

?>
