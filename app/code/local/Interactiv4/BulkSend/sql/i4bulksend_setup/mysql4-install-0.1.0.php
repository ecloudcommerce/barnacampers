<?php

$installer = new Mage_Sales_Model_Mysql4_Setup('sales_setup');

$installer->startSetup();

$installer->addAttribute('order', 'i4bulksend', array('type' => 'int', 'default' => '0', 'grid' => true));

$installer->endSetup();

?>