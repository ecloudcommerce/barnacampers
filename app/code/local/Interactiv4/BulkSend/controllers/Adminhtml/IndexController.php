<?php

/**
 * Description of IndexController
 *
 * @author davidslater
 */
class Interactiv4_BulkSend_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $queuedOrders = array();
        $errors = array();
        try {
            $orderIds = $this->getRequest()->getParam('order_ids') ? $this->getRequest()->getPost('order_ids') : array();
            if (!$orderIds || !is_array($orderIds)) {
                Mage::throwException($this->__("Please select some orders to ship."));
            }
            $orders = Mage::getResourceModel('sales/order_collection'); /* @var $orders Mage_Sales_Model_Resource_Order_Collection */
            $orders->addFieldToFilter("entity_id", array("in" => $orderIds));
            foreach ($orders as $order) { /* @var $order Mage_Sales_Model_Order */
                if (!$this->_getBulkSendHelper()->carrierSupportsBulkSend($order->getShippingCarrier())) {
                    $errors[] = $this->__("Order %s could not be queued for shipment because the shipping carrier does not support that option.", $order->getIncrementId());
                    continue;
                }
                if (!$order->canShip()) {
                    $errors[] = $this->__("Order %s could not be queued for shipment because it cannot be shipped at this time. It may be that all the shipments have already been created.", $order->getIncrementId());
                    continue;
                }
                if ($this->_getBulkSendHelper()->isPendingBulkSend($order)) {
                    $errors[] = $this->__("Order %s could not be queued for shipment because it is already queued.", $order->getIncrementId());
                    continue;
                }
                
                if ($this->_getBulkSendHelper()->bulkSendFailedForOrder($order)) {
                    $errors[] = $this->__("Order %s could not be queued for shipment. It was already queued for shipment but the shipment failed and cannot be retried using this option. Please try to ship the order individually.", $order->getIncrementId());
                    continue;
                }
                
                $this->_getBulkSendHelper()->incrementOrderBulkSendCounter($order);
                $queuedOrders[] = $order->getIncrementId();
            }
        } catch (Mage_Core_Exception $e) {
            $this->_addGlobalError($e->getMessage());
        } catch (Exception $e) {
            $this->_addGlobalError($this->__("An error occurred whilst queueing orders for shipment: '%s'.", $e->getMessage()));
        }

        if ($queuedOrders) {
            $this->_addGlobalSuccess($this->__("The following orders were successfully queued for shipment in one package. You can check whether the shipments have been created yet by clicking <a href='%s'>here</a> or by going to System->Interactiv4 Event Logger->Logs in the menu.<br />%s ", Mage::helper('adminhtml')->getUrl('i4eventlogger/adminhtml_log/index'), implode(" <br />", $queuedOrders)));
        }
        
        if ($errors) {
            $this->_addGlobalError($this->__("The following orders could not be queued for shipment: <br />%s", implode(" <br />", $errors)));
            
        }

        $this->_redirect('adminhtml/sales_order/index');
    }

    /**
     *
     * @return Interactiv4_BulkSend_Helper_Data 
     */
    protected function _getBulkSendHelper() {
        return Mage::helper('i4bulksend');
    }

    /**
     *
     * @param string $errMsg
     * @return \Interactiv4_BulkSend_Adminhtml_IndexController 
     */
    protected function _addGlobalError($errMsg) {
        Mage::getSingleton('core/session')
                ->addError($errMsg);
        return $this;
    }

    /**
     *
     * @param string $successMsg
     * @return \Interactiv4_BulkSend_Adminhtml_IndexController 
     */
    public function _addGlobalSuccess($successMsg) {
        Mage::getSingleton('core/session')
                ->addSuccess($successMsg);
        return $this;
    }

}

?>
