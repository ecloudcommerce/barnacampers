#!/usr/bin/env bash
# Build release Correos Magento Module

prefix_module="Ydral_Correos-"
version=$(grep '<version>' package.xml | sed "s@.*<version>\(.*\)</version>.*@\1@")
module="$prefix_module$version.tgz";
echo $module

tar -czf /tmp/$module app bin js lib skin var package.xml
