jQuery(document).ready(function(){
	FOOTER.init();
	PDP.init();
	CATEGORY.init();
});

var FOOTER = {
	init: function(){
		this.cookies();
		this.MoveCaptcha();
	},
	MoveCaptcha: function(){
		if(jQuery(window).width() < 992){
			jQuery(".block-subscribe .g-recaptcha").insertAfter(".block-subscribe .input-box");
		}
		
	},
	newsLeave: function(sender, defaultValue) {
	    if (jQuery.trim(sender.value) == '') sender.value = defaultValue;
	},
	newsSelect: function(sender, defaultValue) {
	    if (sender.value == defaultValue) sender.value = '';
	    else sender.select();
	},
	cookies: function(){
		jQuery('.cookies-consent').click(function() {
			var mensaje = jQuery(this);
					
			var label_width = 26,
			banner_height = 140,
			banner_width = 320 - label_width;
					
			if (mensaje.offset().left <= 0) {
				jQuery(this).animate({left: banner_width}, 400);
				jQuery('.cookies-explanation').animate({left: 0}, 400);
			} else {
				jQuery(this).animate({left: 0}, 400);
				jQuery('.cookies-explanation').animate({left: -banner_width}, 400);
				}
					
			});
		},

}
var PDP = {
	init: function(){
		if(jQuery('body').hasClass('catalog-product-view')){
			this.thumb();
		}
	},
	thumb: function(){
		var cant = jQuery(".more-views ul li").length;
		if(cant == 1){
			jQuery(".more-views").css("display","none");
		}
	}

}
var CATEGORY = {
	init: function(){
		if(jQuery('body').hasClass('catalog-category-view')){
			this.MoveElements();
		}
	},
	MoveElements: function(){
		jQuery(".block-layered-nav.block-layered-nav--no-filters").add(".block-layered-nav").insertBefore(".col-left.sidebar");
	}
}



