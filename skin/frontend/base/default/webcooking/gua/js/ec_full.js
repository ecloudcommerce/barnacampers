/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2014 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
function sendGuaProductVariantDetails(productConfig) {
    if(!productDetail) {
        return;
    }
    var variant = [];
    var price = parseFloat(productConfig.config.basePrice);
    for(attributeId in productConfig.config.attributes) {
        stateValue = productConfig.state[attributeId];
        for(var i=0; i < productConfig.config.attributes[attributeId].options.length; i++) {
            if(productConfig.config.attributes[attributeId].options[i].id == stateValue) {
                variant.push(productConfig.config.attributes[attributeId].options[i].label);
                price += parseFloat(productConfig.config.attributes[attributeId].options[i].price);
            }
        }
    }
    productDetail.variant = variant.join(' - ');
    productDetail.price = price;
    
    ga('ec:addProduct', productDetail);
    ga('ec:setAction', 'detail');
    ga('send', 'event', 'click', 'variant', productDetail.id + ' ' + productDetail.variant);
}
function guaOnProductClick(event) {
    var productField = {};
    productField['id'] = $(this).getAttribute('data-gua-ec-id');
    productField['name'] = $(this).getAttribute('data-gua-ec-name');
    productField['category'] = $(this).getAttribute('data-gua-ec-category');
    productField['brand'] = $(this).getAttribute('data-gua-ec-brand');
    productField['variant'] = $(this).getAttribute('data-gua-ec-variant');
    productField['position'] = $(this).getAttribute('data-gua-ec-position');
    productField['price'] = $(this).getAttribute('data-gua-ec-price');
    if(productField['id'] || productField['name']) {
        var href = $(this).getAttribute('href');
        var listValue = $(this).getAttribute('data-gua-ec-list');
        if(!listValue) {
            listValue = 'Product list';
        }
        ga('ec:addProduct', productField);
        ga('ec:setAction', 'click', {list: listValue});
        ga('send', 'event', 'click', 'product', productField['id'] + ' ' + productField['name'], {
            'hitCallback': function() {
              document.location = href;
            }
        });
        event.stop();
    }
}

function guaOnPromoClick(event) {
    var promoField = {};
    promoField['id'] = $(this).getAttribute('data-gua-ec-promo-id');
    promoField['name'] = $(this).getAttribute('data-gua-ec-promo-name');
    promoField['creative'] = $(this).getAttribute('data-gua-ec-promo-creative');
    promoField['position'] = $(this).getAttribute('data-gua-ec-promo-position');
    if(promoField['id'] || promoField['name']) {
        var href = $(this).getAttribute('href');
        ga('ec:addPromo', promoField);
        ga('ec:setAction', 'promo_click');
        ga('send', 'event', 'click', 'promo', promoField['id'] + ' ' + promoField['name'], {
            'hitCallback': function() {
              document.location = href;
            }
        });
        event.stop();
    }
}
Event.observe(window, 'load', function() {
    $$('a[data-gua-ec-id]').invoke('observe', 'click', guaOnProductClick);
    $$('a[data-gua-ec-promo-id]').invoke('observe', 'click', guaOnPromoClick);
});